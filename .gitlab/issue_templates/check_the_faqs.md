Thank you for using the Modding-OpenMW.com issue tracker!

Before submitting a new issue, please check the mod list FAQ page in case the issue you're about to report is a known thing:

* [Mod List FAQ](https://modding-openmw.com/mod-list-faq/)

Please also be sure to use the search function [on the issues page](https://gitlab.com/modding-openmw/modding-openmw.com/-/issues) to search for the thing you're about to report, in case it's already been reported.

If your issue isn't covered by the FAQ and is not already reported, please feel free to describe it below and include all relevant details:

* What mod list are you using?

* Does your issue concern a specific mod or mods?

* If you've found inaccurate information, please describe what needs updating.

* Are there specific steps involved in recreating your issue?
