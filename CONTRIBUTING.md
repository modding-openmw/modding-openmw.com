# Developers\' Guide

Are you interested in helping build out this website? This page will
help you get started!

## Table of contents

-   [Prerequisites](#prerequisites)
-   [Get The Source](#get-source)
-   [Setting Up The Website](#setting-up-the-website)
-   [Running The Website](#running-the-website)
-   [Running The Tests](#running-tests)
-   [Making A Change To The Website](#making-changes)

### [Prerequisites](#prerequisites)

The following are required to follow this guide:

- Python

- Firefox

- [Git](https://git-scm.com/)

- Optional: GNU make

- Optional: For Windows users, WSL (alternatively, instead of running the
make commands, looking into the Makefile and run the appropriate
command).

The website is written with the [Django web framework](https://www.djangoproject.com/), which is itself written with
[Python](https://www.python.org/). Knowledge of Python and/or web
programming are nice, but not strictly required to contribute. Knowledge
of how to use git is very beneficial.

It's also a good idea to get yourself a text editor that is aware of:
Python, HTML, CSS, Javascript, YAML, and TOML

The [Dockerfile](./Dockerfile) is used for CI.

### [Get The Source](#get-source) {#get-source}

You can download or clone the website source code [from
GitLab](https://gitlab.com/modding-openmw/modding-openmw.com).

    cd /some/path
    git clone https://gitlab.com/modding-openmw/modding-openmw.com

### [Setting Up The Website](#setting-up-the-website)

    make python-setup

To reset the seed data:

    make reset

### [Running The Website](#running-the-website)

To run the website from your local source code:

    make server

You can then visit <http://localhost:8000> to view the site locally.

### [Running Tests](#running-tests)

To run source code tests from your local source code:

    make test-python

### [Making A Change To The Website](#making-changes) {#making-changes}

If you have a change you\'d like to see merged into the website
codebase, the process looks something like this:

- Create a GitLab account if you don\'t already have one.
- Fork the website source code
- Push your changes to your fork
- Open a merge request to the `beta` branch

A website maintainer will be notified of your merge request and review
it. From there, the path to merging depends on the nature of your change
and the availability of maintainers to review and test the changes.
