from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.urls.base import reverse
from .managers import (
    DraftMediaTrackManager,
    HiddenMediaTrackManager,
    LiveMediaTrackManager,
)


try:
    USE_ASCIINEMA = settings.USE_ASCIINEMA
except AttributeError:
    USE_ASCIINEMA = False


class MediaTrack(models.Model):
    LIVE = 1
    DRAFT = 2
    HIDDEN = 3
    STATUS_CHOICES = ((LIVE, "Live"), (DRAFT, "Draft"), (HIDDEN, "Hidden"))
    ASCIINEMA = 1
    AUDIO = 2
    VIDEO = 3
    IMAGE = 4

    def _get_type_choices(asciinema, audio, video, image):
        if USE_ASCIINEMA:
            return (
                (asciinema, "Asciinema"),
                (audio, "Audio"),
                (video, "Video"),
                (image, "Image"),
            )
        return ((audio, "Audio"), (video, "Video"), (image, "Image"))

    TYPE_CHOICES = _get_type_choices(ASCIINEMA, AUDIO, VIDEO, IMAGE)
    objects = models.Manager()
    live = LiveMediaTrackManager()
    draft = DraftMediaTrackManager()
    hidden = HiddenMediaTrackManager()
    author = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    description = models.TextField()
    media_type = models.IntegerField(choices=TYPE_CHOICES, default=AUDIO)
    other_html = models.CharField(
        help_text="For things like YouTube video links and etc.",
        null=True,
        max_length=400,
    )
    poster = models.CharField(null=True, max_length=250)
    date_added = models.DateTimeField()
    date_updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=DRAFT)
    title = models.CharField(max_length=200)
    url = models.URLField(help_text="URL of the file.")

    class Meta:
        ordering = ["-date_added"]

    def __str__(self):
        return self.title

    def audio_html(self):
        if self.media_type == self.AUDIO:
            audio = """
<audio src="{0}" controls>
 <a href="{0}">Download</a>
</audio>""".format(
                self.url
            )
            return audio
        else:
            return None

    def asciinema_js(self):
        if self.media_type == self.ASCIINEMA:
            js = """asciinema.player.js.CreatePlayer(
'player-container',
'{0}',
{{
  title:      "{1}"
}});""".format(
                self.url, self.title
            )
            return js
        else:
            return None

    def image_html(self):
        if self.media_type == self.IMAGE:
            return """<a href="{0}"><img width="768" height="432" style="padding-bottom: 30px;" class="center" src="{0}"></a>""".format(
                self.url
            )
        else:
            return None

    def video_html(self):
        if self.media_type == self.VIDEO:
            video = """
<video width="768" height="432" controls poster="{poster}">
  <source src="{url}" type="video/mp4">
Your browser does not support the video tag, so you can't watch this.
</video>""".format(
                poster=self.poster or "", url=self.url
            )
            return video
        else:
            return None

    def get_absolute_url(self):
        return reverse("media:detail", kwargs={"slug": self.slug})

    def get_status_string(self):
        return self.STATUS_CHOICES[self.status - 1][1]

    def get_media_type_string(self):
        return self.TYPE_CHOICES[self.media_type - 1][1]

    @property
    def media_link_300200(self):
        if self.media_type == self.AUDIO:
            return None
        elif self.media_type == self.ASCIINEMA:
            return None
        elif self.media_type == self.IMAGE:
            return """<img width="300" height="200" class="center" src="{}">""".format(
                self.url.replace("/images/", "/images/300200_")
            )
        elif self.media_type == self.VIDEO:
            return """<video src="{}" type="video/mp4"></video>""".format(self.url)
