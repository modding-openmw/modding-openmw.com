from django.contrib.auth.models import AnonymousUser
from django.contrib.sitemaps.views import sitemap
from django.shortcuts import reverse
from django.test import RequestFactory, TestCase
from ..models import Mod, Category, Tag
from ..views.utility import static_view, webmanifest
from ..views.dynamicpages import (
    all_mods,
    fully_working,
    partial_working,
    not_working,
    compat_unknown,
    user_settings,
    mod_list_detail,
    mod_list_changelog,
    mod_list_final,
    users_guide,
    auto_guide,
    mod_lists,
    mod_list_step_detail,
    mod_alts,
    categories,
    category_detail,
    latest_mods,
    tags,
    tag_detail,
    no_plugin,
    with_plugin,
    example_mod_page,
    mod_detail,
)
from ..views.forms import cfg_blank, cfg_generator
from ..views.search import search
from ..sitemaps import sitemap_dict

ALL_LIST_SLUGS = (
    "i-heart-vanilla",
    "i-heart-vanilla-directors-cut",
    "just-good-morrowind",
    "one-day-morrowind-modernization",
    "graphics-overhaul",
    "expanded-vanilla",
    "total-overhaul",
    "total-overhaul-wip",
    "starwind-modded",
)
HTTP_OK = 200


class MomwTestCase(TestCase):
    fixtures = ["test-data.json"]  # Usually created with `make dumpdata`

    @classmethod
    def setUpTestData(cls):
        cls.cat1 = Category(title="Test Category 1", slug="test-category-1")
        cls.tag1 = Tag(name="TEST TAG1", slug="test-tag-1")
        cls.mod1 = Mod(
            name="Test Mod 1",
            author="Test Mod Author",
            description="Test Mod 1 Description",
            category=cls.cat1,
            # compat_link="dafuq",
            # alt_to
            date_added="2023-09-02 11:57:00 -0500",
            date_updated="2023-09-02 11:57:00 -0500",
            dl_url="DL URL FIELD",
            tags=[cls.tag1],
            slug="test-mod-1",
        )

    def setUp(self):
        self.f = RequestFactory()

    def test_cat_get_absolute_url(self):
        self.assertEqual(
            self.cat1.get_absolute_url(), "/mods/category/test-category-1/"
        )

    def test_mod_clean_name(self):
        self.assertEqual(self.mod1.clean_name, "TestMod1")

    def test_mod_compat_string(self):
        self.assertEqual(self.mod1.compat_string, "Fully Working")

    def test_mod_compat_link(self):
        self.assertEqual(
            self.mod1.compat_link,
            '<a href="/compatibility/#fully-working">Fully Working</a>',
        )

    def test_mod_dl_item(self):
        self.assertEqual(self.mod1.dl_item(), "DL URL FIELD")

    def test_mod_get_absolute_url(self):
        self.assertEqual(self.mod1.get_absolute_url(), "/mods/test-mod-1/")

    def test_tag_get_absolute_url(self):
        self.assertEqual(self.tag1.get_absolute_url(), "/mods/tag/test-tag-1/")

    def test_about_page(self):
        r = self.f.get(reverse("about"))
        r.user = AnonymousUser()
        response = static_view(r, "about.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_changelogs_website(self):
        r = self.f.get(reverse("changelogs-website"))
        r.user = AnonymousUser()
        response = static_view(r, "changelogs-website.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_index(self):
        r = self.f.get(reverse("index"))
        r.user = AnonymousUser()
        response = static_view(r, "index.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_webmanifest(self):
        r = self.f.get(reverse("webmanifest"))
        r.user = AnonymousUser()
        response = webmanifest(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_changelogs(self):
        r = self.f.get(reverse("changelogs"))
        r.user = AnonymousUser()
        response = static_view(r, "changelogs.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_compatibility(self):
        r = self.f.get(reverse("compatibility"))
        r.user = AnonymousUser()
        response = static_view(r, "compatibility.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_compatibility_fully_working(self):
        r = self.f.get(reverse("fully_working"))
        r.user = AnonymousUser()
        response = fully_working(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_compatibility_partial_working(self):
        r = self.f.get(reverse("partial_working"))
        r.user = AnonymousUser()
        response = partial_working(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_compatibility_not_working(self):
        r = self.f.get(reverse("not_working"))
        r.user = AnonymousUser()
        response = not_working(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_compatibility_unknown(self):
        r = self.f.get(reverse("compat_unknown"))
        r.user = AnonymousUser()
        response = compat_unknown(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_user_settings(self):
        r = self.f.get(reverse("user_settings"))
        r.user = AnonymousUser()
        response = user_settings(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_subscribe(self):
        r = self.f.get(reverse("subscribe"))
        r.user = AnonymousUser()
        response = static_view(r, "subscribe.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_cookie(self):
        r = self.f.get(reverse("cookie"))
        r.user = AnonymousUser()
        response = static_view(r, "cookie.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_enable_in_cfg_generator_button(self):
        r = self.f.get(reverse("enable-in-cfg-generator-button"))
        r.user = AnonymousUser()
        response = static_view(r, "enable-in-cfg-generator-button.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_faq(self):
        r = self.f.get(reverse("faq"))
        r.user = AnonymousUser()
        response = static_view(r, "faq.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_faq_tooling(self):
        r = self.f.get(reverse("faq-tooling"))
        r.user = AnonymousUser()
        response = static_view(r, "faq-tooling.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_faq_mod_list(self):
        r = self.f.get(reverse("mod-list-faq"))
        r.user = AnonymousUser()
        response = static_view(r, "mod-list-faq.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_getting_started(self):
        r = self.f.get(reverse("getting_started"))
        r.user = AnonymousUser()
        response = static_view(r, "getting_started.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_getting_started_buy(self):
        r = self.f.get(reverse("gs-buy"))
        r.user = AnonymousUser()
        response = static_view(r, "gs_buy.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_getting_started_install(self):
        r = self.f.get(reverse("gs-install"))
        r.user = AnonymousUser()
        response = static_view(r, "gs_install.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_getting_started_settings(self):
        r = self.f.get(reverse("gs-settings"))
        r.user = AnonymousUser()
        response = static_view(r, "gs_settings.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_getting_started_tips(self):
        r = self.f.get(reverse("gs-tips"))
        r.user = AnonymousUser()
        response = static_view(r, "gs_tips.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_guides(self):
        r = self.f.get(reverse("guides"))
        r.user = AnonymousUser()
        response = static_view(r, "guides.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_guides_developers(self):
        r = self.f.get(reverse("guides-developers"))
        r.user = AnonymousUser()
        response = static_view(r, "guides_developers.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_guides_modders(self):
        r = self.f.get(reverse("guides-modders"))
        r.user = AnonymousUser()
        response = static_view(r, "guides_modders.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_guides_users(self):
        r = self.f.get(reverse("guides-users"))
        r.user = AnonymousUser()
        response = users_guide(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_guides_auto(self):
        r = self.f.get(reverse("guides-auto"))
        r.user = AnonymousUser()
        response = auto_guide(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_guides_auto_all(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(reverse("guides-auto"), kwargs={"slug": slug})
            r.user = AnonymousUser()
            response = auto_guide(r)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_lists(self):
        r = self.f.get(reverse("mod-lists"))
        r.user = AnonymousUser()
        response = mod_lists(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_lists_json(self):
        r = self.f.get(reverse("mod-lists"), kwargs={"as_json": True})
        r.user = AnonymousUser()
        response = mod_lists(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_lists_add(self):
        r = self.f.get(reverse("add-a-list"))
        r.user = AnonymousUser()
        response = static_view(r, "add_a_list.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_list_detail_all(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(reverse("mod-list-detail", kwargs={"slug": slug}))
            r.user = AnonymousUser()
            response = mod_list_detail(r, slug=slug)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_list_detail_all_json(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(reverse("mod-list-detail", kwargs={"slug": slug}))
            r.user = AnonymousUser()
            response = mod_list_detail(r, slug=slug, as_json=True)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_list_detail_all_changelog(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(reverse("mod-list-changelog", kwargs={"slug": slug}))
            r.user = AnonymousUser()
            response = mod_list_changelog(r, slug=slug)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_list_detail_all_final(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(reverse("mod-list-final", kwargs={"slug": slug}))
            r.user = AnonymousUser()
            response = mod_list_final(r, slug=slug)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_list_detail_all_step_one(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(
                reverse("mod-list-step-detail", kwargs={"slug": slug, "step": 1})
            )
            r.user = AnonymousUser()
            response = mod_list_step_detail(r, slug=slug, step=1)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_list_detail_all_step_one_json(self):
        for slug in ALL_LIST_SLUGS:
            r = self.f.get(
                reverse("mod-list-step-detail", kwargs={"slug": slug, "step": 1})
            )
            r.user = AnonymousUser()
            response = mod_list_step_detail(r, slug=slug, step=1, as_json=True)
            self.assertEqual(response.status_code, HTTP_OK)

    def test_load_order(self):
        r = self.f.get(reverse("load_order"))
        r.user = AnonymousUser()
        response = static_view(r, "load_order.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_index(self):
        r = self.f.get(reverse("mod_index"))
        r.user = AnonymousUser()
        response = all_mods(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_all_mods(self):
        r = self.f.get(reverse("all_mods"))
        r.user = AnonymousUser()
        response = all_mods(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_alts(self):
        r = self.f.get(reverse("mod_alts"))
        r.user = AnonymousUser()
        response = mod_alts(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_category_all(self):
        r = self.f.get(reverse("mod_categories"))
        r.user = AnonymousUser()
        response = categories(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_category_detail(self):
        r = self.f.get(reverse("mod_category_detail", kwargs={"slug": "leveling"}))
        r.user = AnonymousUser()
        response = category_detail(r, slug="leveling")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_latest_mods(self):
        r = self.f.get(reverse("latest_mods"))
        r.user = AnonymousUser()
        response = latest_mods(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_tags(self):
        r = self.f.get(reverse("mod_tags"))
        r.user = AnonymousUser()
        response = tags(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_tag_detail(self):
        r = self.f.get(reverse("mod_tag_detail", kwargs={"slug": "openmw-lua"}))
        r.user = AnonymousUser()
        response = tag_detail(r, slug="openmw-lua")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mods_no_plugin(self):
        r = self.f.get(reverse("no_plugin"))
        r.user = AnonymousUser()
        response = no_plugin(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mods_with_plugin(self):
        r = self.f.get(reverse("with_plugin"))
        r.user = AnonymousUser()
        response = with_plugin(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_example_mod_page(self):
        r = self.f.get(reverse("example-mod-page"))
        r.user = AnonymousUser()
        response = example_mod_page(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_detail_go_home(self):
        r = self.f.get(reverse("mod_detail", kwargs={"slug": "go-home"}))
        r.user = AnonymousUser()
        response = mod_detail(r, slug="go-home")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_mod_detail_go_home_json(self):
        r = self.f.get(reverse("mod_detail", kwargs={"slug": "go-home"}))
        r.user = AnonymousUser()
        response = mod_detail(r, slug="go-home", as_json=True)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_cfg_blank(self):
        r = self.f.get(reverse("cfg_generator"))
        r.user = AnonymousUser()
        response = cfg_blank(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_cfg_generator(self):
        r = self.f.get(reverse("cfg_generator2", kwargs={"preset": "i-heart-vanilla"}))
        r.user = AnonymousUser()
        response = cfg_generator(r, preset="i-heart-vanilla")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_privacy_page(self):
        r = self.f.get(reverse("privacy"))
        r.user = AnonymousUser()
        response = static_view(r, "privacy.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_resources(self):
        r = self.f.get(reverse("resources"))
        r.user = AnonymousUser()
        response = static_view(r, "resources.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_search(self):
        r = self.f.get(reverse("search"))
        r.user = AnonymousUser()
        response = search(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_site_versions(self):
        r = self.f.get(reverse("site-versions"))
        r.user = AnonymousUser()
        response = static_view(r, "site-versions.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_sitemap(self):
        r = self.f.get(reverse("django.contrib.sitemaps.views.sitemap"))
        r.user = AnonymousUser()
        response = sitemap(r, sitemaps=sitemap_dict)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips(self):
        r = self.f.get(reverse("tips"))
        r.user = AnonymousUser()
        response = static_view(r, "tips.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_atlased_meshes(self):
        r = self.f.get(reverse("tips-atlased-meshes"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_atlased_meshes.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_bbc_patching(self):
        r = self.f.get(reverse("tips-bbc-patching"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_bbc_patching.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_creating_custom_groundcover(self):
        r = self.f.get(reverse("tips-creating-custom-groundcover"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_creating_custom_groundcover.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_customizing_modlists(self):
        r = self.f.get(reverse("tips-customizing-modlists"))
        r.user = AnonymousUser()
        response = static_view(r, "tips-customizing-modlists.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_file_renames(self):
        r = self.f.get(reverse("tips-file-renames"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_file_renames.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_ini_importer(self):
        r = self.f.get(reverse("tips-ini-importer"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_ini_importer.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_merging_objects(self):
        r = self.f.get(reverse("tips-merging-objects"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_merging_objects.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_navmeshtool(self):
        r = self.f.get(reverse("tips-navmeshtool"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_navmeshtool.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_openmw_physics_fps(self):
        r = self.f.get(reverse("tips-openmw-physics-fps"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_openmw_physics_fps.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_performance(self):
        r = self.f.get(reverse("tips-performance"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_performance.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_portable_install(self):
        r = self.f.get(reverse("tips-portable-install"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_portable_install.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_register_bsas(self):
        r = self.f.get(reverse("tips-register-bsas"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_register_bsas.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_shiny_meshes(self):
        r = self.f.get(reverse("tips-shiny-meshes"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_shiny_meshes.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_tr_patcher(self):
        r = self.f.get(reverse("tips-tr-patcher"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_tr_patcher.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_tips_custom_shaders(self):
        r = self.f.get(reverse("tips-custom-shaders"))
        r.user = AnonymousUser()
        response = static_view(r, "tips_custom_shaders.html")
        self.assertEqual(response.status_code, HTTP_OK)
