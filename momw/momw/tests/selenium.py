from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from ..models import ModList, Tag


# https://selenium-python.readthedocs.io/api.html#module-selenium.webdriver.common.action_chains
# https://docs.djangoproject.com/en/5.0/topics/testing/tools/#django.test.LiveServerTestCase
class MomwSeleniumFF(StaticLiveServerTestCase):
    fixtures = ["test-data.json"]  # Usually created with `make dumpdata`

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.all_mod_cats = []
        cls.all_mod_lists = []
        cls.all_tags = []
        cls.title_suffix = " | Modding OpenMW: A guide to modding and modernizing Morrowind with OpenMW"
        # Do this so static files load correctly
        settings.DEBUG = True
        for ml in ModList.objects.all():
            if not ml.parent_list and ml.slug != "tes3mp-server":
                cls.all_mod_lists.append(ml)
        for tag in Tag.objects.all():
            cls.all_tags.append(tag)
        cls.ffdriver = webdriver.Firefox()
        cls.ffdriver.implicitly_wait(10)  # Seconds

    @classmethod
    def tearDownClass(cls):
        cls.ffdriver.quit()
        super().tearDownClass()

    def test_index(self):
        self.ffdriver.get(f"{self.live_server_url}/")

    def test_webmanifest(self):
        self.ffdriver.get(f"{self.live_server_url}/manifest.webmanifest")

    def test_changelogs(self):
        self.ffdriver.get(f"{self.live_server_url}/changelogs")

    def test_changelogs_website(self):
        self.ffdriver.get(f"{self.live_server_url}/changelogs/website")

    def test_compatibility(self):
        self.ffdriver.get(f"{self.live_server_url}/compatibility")
        self.ffdriver.get(f"{self.live_server_url}/compat/fully-working")
        self.ffdriver.get(f"{self.live_server_url}/compat/partially-working")
        self.ffdriver.get(f"{self.live_server_url}/compat/not-working")
        self.ffdriver.get(f"{self.live_server_url}/compat/unknown")

    def test_settings(self):
        self.ffdriver.get(f"{self.live_server_url}/settings")

    def test_subscribe(self):
        self.ffdriver.get(f"{self.live_server_url}/subscribe")

    def test_cookie(self):
        self.ffdriver.get(f"{self.live_server_url}/cookie")

    def test_enable_in_cfg_generator_button(self):
        self.ffdriver.get(f"{self.live_server_url}/enable-in-cfg-generator-button")

    def test_faq(self):
        self.ffdriver.get(f"{self.live_server_url}/faq")

    def test_faq_tooling(self):
        self.ffdriver.get(f"{self.live_server_url}/faq/tooling")

    def test_getting_started(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started")

    def test_getting_started_buy(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/buy")

    def test_getting_started_install(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/install")

    def test_getting_started_settings(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/settings")

    def test_getting_started_tips(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/tips")

    def test_guides(self):
        self.ffdriver.get(f"{self.live_server_url}/guides")

    def test_guides_developers(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/developers")

    def test_guides_modders(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/modders")

    def test_guides_auto(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/auto")

    # TODO: Test the folder deploy script download
    def test_guides_users(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/users")

    def test_lists(self):
        self.ffdriver.get(f"{self.live_server_url}/lists")

    def test_lists_json(self):
        self.ffdriver.get(f"{self.live_server_url}/lists/json")

    def test_lists_add(self):
        self.ffdriver.get(f"{self.live_server_url}/lists/add")

    def test_modlist_detail_page(self):
        for ml in self.all_mod_lists:
            self.ffdriver.get(f"{self.live_server_url}/lists/{ml.slug}")
            self.assertEqual(
                self.ffdriver.title,
                f"{ml}{self.title_suffix}",
            )

    def test_load_order(self):
        self.ffdriver.get(f"{self.live_server_url}/load-order")

    # TODO: Media

    def test_mods(self):
        self.ffdriver.get(f"{self.live_server_url}/mods")

    def test_mods_all(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/all")

    def test_mods_alts(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/alts")

    def test_category_all(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/category/all")

    def test_cat_detail_page(self):
        for cat in self.all_mod_cats:
            self.ffdriver.get(f"{self.live_server_url}/mods/category/{cat.slug}")
            self.assertEqual(
                self.ffdriver.title,
                f"{cat}{self.title_suffix}",
            )

    def test_mods_latest(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/latest")

    def test_mods_tag(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/tag")

    def test_tag_detail_page(self):
        for tag in self.all_tags:
            self.ffdriver.get(f"{self.live_server_url}/mods/tag/{tag.slug}")
            self.assertEqual(
                self.ffdriver.title,
                f"{tag}{self.title_suffix}",
            )

    def test_mod_no_plugin(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/no-plugin")

    def test_mod_with_plugin(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/with-plugin")

    def test_example_mod_page(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/example-mod-page")

    def test_go_home_mod_page(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/go-home")
        self.assertEqual(
            self.ffdriver.title,
            f"Go Home!{self.title_suffix}",
        )

    # TODO: test sublist has parent list data
    def test_mod_list_final(self):
        for ml in self.all_mod_lists:
            self.ffdriver.get(f"{self.live_server_url}/lists/{ml.slug}/final")

    # TODO: FF can't open Atom feeds :(
    # def test_feeds_mods(self):
    #     self.ffdriver.get(f"{self.live_server_url}/feeds/mods")
    #     self.assertTrue("<title>Mod Feed</title>" in self.ffdriver.page_source)

    def test_cfg_generator(self):
        self.ffdriver.get(f"{self.live_server_url}/cfg-generator")

    # for preset in (
    #     "expanded-vanilla",
    #     "graphics-overhaul",
    #     "i-heart-vanilla",
    #     "i-heart-vanilla-directors-cut",
    #     "just-good-morrowind",
    #     "one-day-morrowind-modernization",
    #     "starwind-modded",
    #     "total-overhaul",
    # ):
    #     self.ffdriver.get(f"{self.live_server_url}/cfg/{preset}")
    #     # TODO: find something on the page
