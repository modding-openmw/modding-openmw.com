from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from ..models import ModList, ListedMod, UsageNotes

skip_categories = ["settings-tweaks", "tools", "first-steps"]
usage_notes_skip_mods = ["momw-camera"]
skip_mods = []
do_fail = False


def print_list_header(ml):
    print(f"mods on {ml.slug} with issues:")


def print_mod_header(mod, cause=None):
    print(f"\t- {mod.slug}")
    if cause:
        print(f"\t  cause: {cause}")


def do_once(fn, *args):
    done = False

    def _once(*additional):
        nonlocal done
        if not done:
            fn(*args, *additional)
            done = True

    return _once


def check_dl_links(ml, mod, printer):
    if mod.dl_url:
        return True
        # for archive in mod.mod_archives.all():
        #     if not archive.direct_download:
        #         printer("would be nice if this had a propper direct_download but still OK")
        #         return True
    else:
        urlsp = mod.url.split("?")[0]
        if urlsp.endswith(".zip") or urlsp.endswith(".7z") or urlsp.endswith(".rar"):
            return True
        if mod.url.startswith("https://www.nexusmods"):
            return True
        if mod.url.startswith("https://modding-openmw.gitlab.io"):
            return True
        if mod.url.startswith("https://mega.nz"):
            return True
        if mod.url.startswith("https://baturin.org"):
            # for archive in mod.mod_archives.all():
            #     if not archive.direct_download:
            #         printer("would be nice if this had a propper direct_download but still OK")
            #         return True
            return True
        if mod.url.startswith("http://www.tamriel-rebuilt.org"):
            return True
        if mod.url.startswith("https://www.tamriel-rebuilt.org"):
            return True
        if mod.url.startswith("https://www.drive.google.com"):
            return True

        for archive in mod.mod_archives.all():
            if not archive.direct_download:
                printer()
                return False


def check_archives(ml, mod, printer):
    archives = mod.mod_archives.all()

    if len(archives) == 0:
        import pdb

        pdb.set_trace()
        printer("doesn't have any archive :O")
        return False

    if len(archives) > 1:
        # either all archives do have direct_links
        for a in archives:
            if not a.direct_download:
                if not mod.url.startswith("https://www.nexusmods"):
                    printer(
                        "has multiple archives without direct_download set and is not a nexus mod"
                    )
                    return False

    return True


def check_actions(ml, mod, printer):
    if mod.slug in usage_notes_skip_mods:
        return True

    archives = mod.mod_archives.all()
    un = UsageNotes.objects.filter(for_mod=mod, on_lists=ml)

    if (
        len(un) == 1
        and un[0].text
        == "<p><span class='bold'>NOTE: OpenMW 0.49 or newer is required to use this!</span></p>\n"
    ):
        return True
    if len(un) > 0:
        all_actions = []
        for a in archives:
            for action in a.actions.all():
                all_actions.append(action)

        if len(all_actions) == 0:
            printer()
            return False

    return True


class MomwData(StaticLiveServerTestCase):
    fixtures = ["test-data.json"]  # Usually created with `make dumpdata`

    def check(self, check):
        failed = 0
        total = 0
        for ml in ModList.objects.all():
            if not ml.is_parent:
                continue
            print_ml = do_once(print_list_header, ml)

            for lm in ListedMod.objects.filter(modlist__in=ml.modlist_set.all()):
                total += 1
                mod = lm.mod

                if mod.category.slug in skip_categories or mod.slug in skip_mods:
                    continue

                print_mod = do_once(print_mod_header, mod)
                if not check(ml, mod, lambda x=None: print_ml() or print_mod(x)):
                    failed += 1

        return failed, total

    # all non skipped mods should have archives
    def test_archives(self):
        print("Checking archives...")
        failed, total = self.check(check_archives)
        if do_fail:
            self.assertEqual(
                failed, 0, msg=f"{failed} mods of {total} with missing archives"
            )

    # # all mods with usage_notes should have actions (that's so wrong...)
    # def test_actions(self):
    #     print("Checking actions...")
    #     failed, total = self.check(check_actions)
    #     if do_fail:
    #         self.assertEqual(
    #             failed, 0, msg=f"{failed} mods of {total} with missing actions"
    #         )

    # everything with a dl_url should have a proper download_link
    def test_direct_downloads(self):
        print("Checking direct download links...")
        failed, total = self.check(check_dl_links)
        if do_fail:
            self.assertEqual(
                failed,
                0,
                msg=f"{failed} mods of {total} with missing direct download link",
            )
