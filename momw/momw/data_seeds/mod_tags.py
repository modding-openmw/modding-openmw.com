from momw.models import Tag


def mod_tags() -> bool:
    Tag(
        name="Has a BSA",
        description="Mods that provide and require <a href='/tips/register-bsas/'>registering a <code>.bsa</code> file</a>.",
    ).save()
    Tag(name="Normal Maps", description="Mods that use bump or normal maps.").save()
    Tag(name="Specular Maps", description="Mods that use specular maps.").save()
    Tag(name="High Res", description="High resolution meshes and/or textures.").save()
    Tag(
        name="Non-HD",
        description="Meshes or textures that are changed but not made HD.",
    ).save()
    Tag(
        name="Manual Edit Needed",
        description="Needs some kind of manual edit to work properly.",
    ).save()
    Tag(
        name="Vanilla Friendly", description="Doesn't alter the original feel too much."
    ).save()
    Tag(
        name="TES3MP Lua",
        description="These are server-side lua scripts for use with TES3MP.",
    ).save()
    Tag(
        name="Atlased Textures",
        description="These mods use atlased textures, or can be atlased themselves.",
    ).save()
    Tag(
        name="TES3MP: Co-Op",
        description="Scripts that are a good fit for an enhanced co-op experience.",
    ).save()
    Tag(
        name="TES3MP: Dev",
        description="Useful frameworks/libraries for building your own scripts.",
    ).save()
    Tag(
        name="TES3MP: Leveling",
        description="Scripts that alter or enhance the game's leveling systems.",
    ).save()
    Tag(
        name="TES3MP: Gameplay",
        description="Scripts that alter or enhance various gameplay mechanics.",
    ).save()
    Tag(
        name="TES3MP: Difficulty",
        description="Scripts that enhance the difficulty of the game.",
    ).save()
    Tag(
        name="TES3MP: Utility", description="Scripts that aid in server management."
    ).save()
    Tag(
        name="TES3MP: Public Servers",
        description="Scripts that are in use and featured by prominent public servers.",
    ).save()
    Tag(name="TES3MP: v0.6", description="Scripts for TES3MP version 0.6.").save()
    Tag(name="TES3MP: v0.7", description="Scripts for TES3MP version 0.7.").save()
    Tag(
        name="TES3MP Graphics", description="Use these for a TES3MP graphical overhaul."
    ).save()
    Tag(
        name="Building", description="Mods that add building elements to the game."
    ).save()
    Tag(
        name="TES3MP Spawning",
        description="Scripts for managing player spawns and/or chargen.",
    ).save()
    Tag(
        name="Grass",
        description="Mods that add (or optionally add) grass and other groundcover.",
    ).save()
    Tag(
        name="Interesting Additions",
        description="Ambitious, interesting mods that may not fit well with more focused mod lists, but you may want to check out anyways.",
    ).save()
    Tag(
        name="OpenMW Lua",
        description="""Mods that use the OpenMW Lua scripting API. <span class="bold">OpenMW 0.48 or newer is required!</span>""",
    ).save()
    Tag(
        name="OAAB Data",
        description="Mods that require On Ash And Blight (OAAB) assets.",
    ).save()
    Tag(name="Reshade", description="Reshade presets made for OpenMW.").save()
    Tag(
        name="Tamriel Data",
        description="Mods that require Tamriel_Data assets.",
    ).save()
    Tag(
        name="Dev Build Only",
        description="Mods that require the latest OpenMW Developer Build.",
    ).save()
    Tag(
        name="GitLab mods",
        description="Mods hosted on GitLab, either in our <a href='https://gitlab.com/modding-openmw'>Modding-OpenMW organization</a> or otherwise.",
    ).save()
    Tag(
        name="Only On MOMW",
        description="Mods that you can only get via MOMW",
    ).save()
    Tag(
        name="No Mod Manager Download",
        description="Contains optional/archived files or author has exclued mod managers from being an option for a download.",
    ).save()
