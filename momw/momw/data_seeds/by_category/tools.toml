# {% load momwtags %}

[[mod_details]]
name = "Build-OpenMW"
author = """{% nexus_user 'johnnyhostile' %}"""
date_added = "2018-04-13 21:00:00 -0500"
description = """This is a helper script for compiling OpenMW and dependencies from source.  Currently known to support Debian and Void Linux."""
url = """https://github.com/hristoast/build-openmw"""

[[mod_details]]
name = "Delta Plugin"
author = """<a href="https://gitlab.com/bmwinger">Benjamin Winger</a>"""
date_added = "2021-03-15 15:15:50 -0500"
description = """<p>Transcoder for managing a form of markup-based minimal Elder Scrolls Plugin files and OMWAddon files</p>"""
url = """https://gitlab.com/bmwinger/delta-plugin/-/releases"""

[[mod_details]]
name = "Delta Plugin and Groundcoverify"
author = """<a href="https://gitlab.com/bmwinger">Benjamin Winger</a>"""
date_added = "2023-11-09 14:45:00 -0500"
date_updated = "2024-03-30 16:07:00 -0500"
description = """Transcoder for managing a form of markup-based minimal Elder Scrolls Plugin files and OMWAddon files and a simple python script which uses Delta Plugin to turn regular groundcover in morrowind plugins into openmw-style groundcover."""
dl_url = """https://modding-openmw.gitlab.io/momw-tools-pack/"""

[[mod_details]]
name = "Groundcoverify"
author = """<a href="https://gitlab.com/bmwinger">Benjamin Winger</a>"""
date_added = "2023-11-09 14:45:00 -0500"
description = """A simple python script which uses Delta Plugin to turn regular groundcover in morrowind plugins into openmw-style groundcover."""
url = """https://gitlab.com/bmwinger/groundcoverify"""
dl_url = """https://gitlab.com/api/v4/projects/modding-openmw%2Fgroundcoverify/jobs/artifacts/exeify/raw/dist/groundcoverify-win.zip?job=windows"""

[[mod_details]]
name = "Habasi"
author = """{% nexus_user 'alvazir' %}"""
date_added = "2025-01-22 14:23:00 -0500"
description = """Habasi will steal your precious plugins and stash them. It is a command line tool for TES3 plugin merging, e.g. it takes multiple plugins and creates one with their contents."""
url = """{% nexus_mod '53002' %}"""

[[mod_details]]
name = "MGE XE"
author = """{% nexus_user 'Hrnchamd' %}"""
date_added = "2019-02-25 21:15:00 -0500"
description = """<p>Morrowind Graphics Extender XE; allows Morrowind to render distant views, scenery shadows, high quality shaders and other features.</p>"""
url = """{% nexus_mod '41102' %}"""
compat = "not working"

[[mod_details]]
name = "MOMW Configurator"
author = """{% nexus_user 'johnnyhostile' %}"""
tags = ["GitLab mods", "Only On MOMW"]
date_added = "2024-10-08 20:18:00 -0500"
description = """Generates fully functional <code>openmw.cfg</code> and <code>settings.cfg</code> files for any mod list from Modding-OpenMW.com."""
url = """https://modding-openmw.gitlab.io/momw-configurator/"""

[[mod_details]]
name = "MOMW Tools Pack"
author = """fallchildren, johnnyhostile, John Moonsugar, Benjamin Winger, S3ctor"""
tags = ["GitLab mods", "Only On MOMW"]
date_added = "2024-11-04 21:20:00 -0500"
description = """All the tools you need to automatically install and configure Modding-OpenMW.com mod lists in one package."""
url = """https://modding-openmw.gitlab.io/momw-tools-pack/"""

[[mod_details]]
name = "MW Mesh Generator"
author = """{% nexus_user 'Yacoby' %}"""
date_added = "2025-01-22 14:23:00 -0500"
description = """"""
url = """{% nexus_mod '23065' %}"""

[[mod_details]]
name = "OMWLLF"
author = """<a href="https://github.com/jmelesky">John Melesky</a>"""
date_added = "2018-04-21 21:00:00 -0500"
date_updated = "2021-01-24 15:25:09 -0500"
description = """This tool is absolutely essential if you use a mod that deals with leveled lists, which is very common.  If you don't know what that is, and you use more than a handful of mods with plugins, it is probably worthwhile to use this."""
url = """https://github.com/jmelesky/omwllf/blob/master/README.md"""

[[mod_details]]
name = "OpenMW AppImage"
author = "OpenMW Contributors"
date_added = "2022-11-19 10:27:00 -0500"
description = """A portable Linux executable for OpenMW Launcher, OpenMW CS, Navmeshtool, and the OpenMW engine executable. Source and build instructions can be found <a href='https://gitlab.com/modding-openmw/openmw-appimage#openmw-appimage'>on GitLab</a>."""

[[mod_details]]
name = "OpenMW Lua helper utility"
author = """{% nexus_user 'taitechnic' %}"""
date_added = "2025-01-22 14:23:00 -0500"
description = """Provides a bridge to MWscript for a few bits of functionality currently missing in OpenMW lua. The most useful function currently is the simulation of MWSE style <code>infoGetText</code> events, so a lua mod can be notified when Dialogue topics are clicked on."""
url = """{% nexus_mod '54629' %}"""

[[mod_details]]
name = "OpenMW-ModChecker"
author = """{% nexus_user 'johnnyhostile' %}"""
date_added = "2018-07-04 21:00:00 -0500"
description = """This has been superceded by my new <a href="/mods/openmw-validator/"><code>openmw-validator</code></a> tool! Please check that out instead, this is no longer an active project. This command-line script will determine if a loaded mod is made obsolete by another mod later in the load order. I wrote this script to scan my own load order to determine if any mods in this site's recommended list were in fact not needed at all due to being overwritten.  This should be useful for anyone who's got a mod list that includes things not explicitly listed in the recommended list."""
url = """https://github.com/hristoast/OpenMW-ModChecker"""

[[mod_details]]
name = "OpenMW-Nix"
author = "PopeRigby"
date_added = "2024-07-30 20:02:00 -0700"
description = """A Nix flake containing useful tools for playing OpenMW, including a nightly package for OpenMW"""
url = """https://codeberg.org/PopeRigby/openmw-nix"""

[[mod_details]]
name = "OpenMW Quick switch between settings"
author = """{% nexus_user 'napalmstrike2007' 'napstrike' %}"""
date_added = "2021-04-11 11:13:10 -0500"
description = """<p>This is a windows batch script that quickly switches between two versions of files. The second versions should have the prefix "transpose_"(without quotes).</p>"""
url = """{% nexus_mod '49543' %}"""

[[mod_details]]
name = "Test Script Lite"
author = """<a href="https://gitlab.com/Settyness/">Settyness</a>"""
tags = ["GitLab mods"]
date_added = "2024-06-28 03:32:00 -0500"
description = """This is a startup test script for OpenMW that may be helpful for testing your setup. There exists yet more variants of this script <a href="https://gitlab.com/Settyness/morrowind-big-brain-baubles/-/tree/main/TestScripts" title="TestScripts — Big Brain Baubles">here</a> with varying object sets for various purposes."""
url = """https://gitlab.com/Settyness/morrowind-big-brain-baubles/-/blob/main/TestScripts/test-script-lite.txt"""

[[mod_details]]
name = "openmw-validator"
author = """{% nexus_user 'johnnyhostile' %}"""
tags = ["GitLab mods", "Only On MOMW"]
date_added = "2021-01-16 16:56:33 -0500"
description = """An <code>openmw.cfg</code> validation tool for modders to double check their work."""
url = """https://modding-openmw.gitlab.io/openmw-validator/"""

[[mod_details]]
name = "Project Atlas Scripts"
author = """{% nexus_user 'johnnyhostile' %}"""
date_added = "2019-03-10 19:03:00 -0500"
description = """<p>Bash script variant of the <code>.bat</code> files that come with <a href="/mods/project-atlas/">Project Atlas</a> and compatible mods, for those of us that aren't using Windows.</p>"""
url = """https://github.com/hristoast/atlas-project-scripts"""

[[mod_details]]
name = "S3LightFixes"
author = """S3ctor"""
date_added = "2024-12-13 20:59:00 -0500"
description = """S3LightFixes is a descendant of Waza-lightfixes, which itself is a descendant of Lightfixes.pl by vtastek. All three applications are designed to make ESP files which adjust the lighting values from all mods listed in one's openmw.cfg. S3LightFixes will handling negative lighting for you, a seperate solution is not needed in per-pixel lighting setups."""
url = """https://github.com/magicaldave/S3LightFixes/releases"""

[[mod_details]]
name = "tes3cmd"
author = """<a href="https://github.com/john-moonsugar">John Moonsugar</a>"""
date_added = "2018-12-01 10:00:00 -0500"
date_updated = "2021-11-10 17:37:04 -0500"
description = """A command line tool to examine and manipulate plugins for the Elder Scrolls game Morrowind."""
# url = """http://www.abitoftaste.altervista.org/morrowind/index.php?option=downloads&Itemid=50&task=info&id=103"""
dl_url = """/files/tes3cmd-0.37v-2013.10.06.7z"""

[[mod_details]]
name = "umo"
author = "fallchildren"
tags = ["GitLab mods", "Only On MOMW"]
date_added = "2024-08-11 22:15:23 +0100"
description = """umo is an automatic modlist downloader for Modding-OpenMW.com."""
url = """https://modding-openmw.gitlab.io/umo/"""

[[mod_details]]
name = "vtasteks-light-fixes.sh"
author = """{% nexus_user 'johnnyhostile' %}"""
date_added = "2021-01-24 15:15:50 -0500"
description = """Export your OpenMW load order to a folder that you can use with <code>tes3cmd</code>. Useful for things like <a href="/mods/vtasteks-shaders/">Vtastek's Shaders</a>' LightFixes patch."""

[[mod_details]]
name = "waza_lightfixes"
author = """{% nexus_user 'wazabear' %}"""
date_added = "2023-11-09 15:21:00 -0500"
description = """This is a tool which generates a plugin which fixes lights."""
url = """https://github.com/glassmancody/waza_lightfixes/releases"""
alt_to = ["S3LightFixes"]
