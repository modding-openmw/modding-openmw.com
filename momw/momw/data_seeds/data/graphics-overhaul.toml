title = "Graphics Overhaul"
description = """<p>This is the graphics-focused version of the <a href='{% url 'mod-list-detail' slug='total-overhaul' %}'>Total Overhaul</a> list, with a greatly reduced number of extra quest content. Included are: mesh and texture replacers, plugins that alter scenes (including adding clutter), effects replacers, landmasses, and more.</p>"""
short_description = "This is the graphics-only version of the <a href='{% url 'mod-list-detail' slug='total-overhaul' %}'>Total Overhaul</a> list. All the graphical replacers, none of the gameplay modifications."

[[sublists]]
title = "First Steps"
description = "These are all the steps you need to follow before installing a mod list"
mods = [
  "Buy and Install Morrowind",
  "Install OpenMW",
  "Set your Site Settings",
  "Folder Deploy Script",
  "How to Install Mods",
]

[[sublists]]
title = "Patches"
description = "Fixes the vast majority of gameplay issues with the base game, and the Tribunal and Bloodmoon expansions. Highly recommended."
mods = [
  "Patch for Purists",
]

[[sublists]]
title = "Modding Resources"
description = "Community asset repositories, used by many other mods."
mods = [
  "Tamriel Data",
  "OAAB_Data",
]

[[sublists]]
title = "Consistency"
description = "Mods that fix inconsistencies in the Morrowind game world."
mods = [
  "Expansion Resource Conflicts",
]

[[sublists]]
title = "Land & Landmass Additions"
description = "Add large amounts of extra land, mostly so you there's something to see across the sea from Vvardenfell. The mainland also offers plenty of new content if you're interested."
mods = [
  "Tamriel Rebuilt",
  "Solstheim Tomb of the Snow Prince",
]

[[sublists]]
title = "Meshes & Performance"
description = "Mesh replacers. Most of these correct mistakes in the originals, often resulting in performance increases, others will affect gameplay (Graphic Herbalism)."
mods = [
  "Mesh Fix v1.2",
  "Correct Meshes",
  "Morrowind Optimization Patch",
  "Graphic Herbalism - MWSE and OpenMW Edition",
  "Correct UV Rocks",
  "Properly Smoothed Meshes",
  "Project Atlas",
  "RR Mod Series - Better Meshes",
  "Jammings Off",
]

[[sublists]]
title = "Texture Packs"
description = "These provide large amounts of textures, covering all assets in the game or close to it."
mods = [
  "Morrowind Enhanced Textures",
  "OAAB Full Upscale",
  "Normal Maps for Morrowind",
  "Normal Maps for Everything",
  "V.I.P. - Vegetto's Important Patches",
]

[[sublists]]
title = "Landscapes"
description = "High quality texture replacements for large amounts of the games landscapes."
mods = [
  "Tyddy's Landscape Retexture",
  "Landscape Retexture",
  "Apel's Asura Coast and Sheogorath Region Retexture",
  "Tamriel Rebuilt Terrain Normal Height Maps for OpenMW",
]

[[sublists]]
title = "Architecture"
description = "New meshes and textures for general architecture."
mods = [
  "Dwemer Mesh Improvement",
  "Dwemer Mesh Improvements Revamped",
  "Full Dwemer Retexture",
  "Full Dwemer Retexture - Normal Maps",
  "Dwemer Lightning Rods",
  "OAAB Dwemer Pavements",
  "Daedric Ruins - Arkitektora of Vvardenfell",
  "Daedric Ruins (Half-Revamped)",
  "Imperial Towns Normal Mapped for OpenMW",
  "Imperial Forts Normal Mapped for OpenMW",
  "Shacks Docks and Ships - Arkitektora of Vvardenfell",
  "Sewers - Arkitektora of Vvardenfell",
  "Aesthesia - Stronghold textures",
  "Aesthesia - Stronghold Normal Maps",
  "Ghastly Glowyfence",
  "RR Mod Series - Morrowind Statues Replacer",
  "Baar Dau - Ministry of Truth",
  "Ministry of truth Bump mapped",
  "Ashlanders textures",
  "Vivec and Velothi - Arkitektora Vol.2",
  "Hlaalu - Arkitektora Vol.2",
  "Arkitektora White Suran",
  "Redoran - Arkitektora Vol.2",
  "Telvanni Mesh Improvement",
  "Telvanni - Arkitektora of Vvardenfell",
  "Necrom - Arkitektora of Morrowind",
  "Vivec and Velothi - Arkitektora Vol.2 Normal Maps",
  "Hlaalu - Arkitektora Vol.2 Normal Maps",
  "Redoran - Arkitektora Vol.2 Normal Maps",
]

[[sublists]]
title = "Flora & Foliage"
description = "Update the look of flora and foliage via improved meshes and high quality textures."
mods = [
  "Epic Plants",
  "Hackle-lo Fixed",
  "Thickle-Lo - The Succulent Hackle-Lo Mod",
  "Comberry Bush and Ingredient Replacer",
  "Pherim's Fire Fern - Plant and Ingredient",
  "Mid Mushroom Overhaul",
  "Vibrant Ivy and Trellis Additions",
  "Scum Retexture",
  "Scummy Scum",
  "Perfect Scum",
]

[[sublists]]
title = "Trees"
description = "Replace the stock trees with more dense and detailed assets."
mods = [
  "SM Bitter Coast Tree Replacer",
  "Melchior's Excellent Grazelands Acacia",
  "Vanilla-friendly West Gash Tree Replacer",
  "Remiros' Ascadian Isles Trees 2",
]

[[sublists]]
title = "Nature"
description = "Update the look various natural objects via improved meshes and high quality textures."
mods = [
  "Caverns Bump Mapped",
  "Caverns Bump Mapped Fix",
  "Apel's Fire Retexture Patched for OpenMW and Morrowind Rebirth",
  "Pimp my Lava",
  "Subtle Smoke",
  "Better Waterfalls",
  "Nocturnal Moths",
  "Fireflies",
  "Cave Drips",
]

[[sublists]]
title = "Environments"
description = "Update the visuals of stock objects in the environment."
mods = [
  "Welcome Road Marker Retexture - Aestetika of Vvardenfell",
  "Signposts Replacer for Tamriel Rebuilt and Vvardenfell",
  "Apel's Various Things - Signs",
  "Unique Tavern Signs for Tamriel Rebuilt",
  "Balmora Road Normal Maps",
  "Swayland",
]

[[sublists]]
title = "Sky & Weather"
description = "Alter the looks and behavior of the sky, clouds, and weather."
mods = [
  "Skies .IV",
  "Oh God Snow for Skies.iv and OpenMW",
  "New Starfields",
  "Meteors",
]

[[sublists]]
title = "Lighting"
description = "Update the behavior and look of lighting in the game, going for a darker and more accurate look overall."
mods = [
  "Glow in the Dahrk",
  "Nords shut your windows",
  "Magical lights for Telvanni",
  "Darknut's Lighted Dwemer Towers",
  "Improved Lights for All Shaders",
  "OpenMW vanilla candles patched with Enlightened Flames and ILFAS",
  "Dunmer Lanterns Replacer",
  "Logs on Fire",
]

[[sublists]]
title = "Clutter & Items"
description = "Updated assets for the very large number of clutter and other various items in the game."
mods = [
  "Remiros' Mod Graveyard",
  "Silverware Repolished",
  "Ingredients Mesh Replacer",
  "Detailed Tapestries",
  "AST Beds Remastered",
  "Better Kegstands",
  "Guar Skin Banners for OpenMW and Vanilla Morrowind",
  "Improved Kwama Eggs and Egg Sacs",
  "One True Faith - Saints and Frescoes Retexture",
  "Dunmeri Urns - Aestetika of Vvardenfell",
  "Long Live The Limeware - Retexture",
  "Long Live The Glassware - Retexture",
  "Long Live The Plates - Retexture",
  "Improved Better Skulls",
  "R-Zero's Random Retextures (and Replacers)",
  "EKM Vanilla-Based Paper Lanterns",
  "Ket's Potions and Beverages Retexture",
  "Ket's Swirlwood Furniture Retexture",
  "All Books Color-Coded and Designed",
  "OpenMW Containers Animated",
  "Apel's Various Things - Sacks",
  "Septim Gold and Dwemer Dumacs",
  "Crystal Soul Gems",
  "FM - Unique Items Compilation",
  "Know Thy Ancestors",
  "Dunmers ashpits",
  "Salts",
  "Store Entrance Chimes",
  "HD Forge",
  "6th House - Glowing Things",
  "HD Flags and Banners",
  "Better Telvanni Crystal",
  "Normal and Specular maps for Better Telvanni Crystal",
  "Temple Shrines Glow",
  "Pherim's Small and WIP Mods",
]

[[sublists]]
title = "Clothing"
description = "Higher quality clothing, and while we're at it, remove the enchanted glow \"effect\"."
mods = [
  "Better Clothes Complete",
  "Better Robes",
  "Better Robes - Updated Tamriel Rebuilt Patch",
  "New Gondolier Helm Fixed for OpenMW",
  "Common Shoe Pack",
  "Mage Robes",
  "Telvanni Magister Robes",
]

[[sublists]]
title = "Armor"
description = "An updated look for the game's armor, with OpenMW's modern features in mind."
mods = [
  "HiRez Armors - Native Styles V2 Fixed and Optimized",
  "HiRez Armors Native Styles V2 for OpenMW",
  "Armors Retexture - Outlander Styles",
  "Daedric Lord Armor Morrowind Edition",
  "Orcish Retexture",
  "New Fists of Randagulf",
  "New Lord's Mail",
  "More Realistic Dwemer Armor",
  "The Forgotten Shields - Artifacts",
  "Ebony Mail (Replacer)",
  "SM Mask of Dagoth Ur",
  "Glass Glowset",
  "More Glowing Mods",
  "Bear Armor Replacer",
]

[[sublists]]
title = "Weaponry"
description = "An update treatment for weapons, covering many uniques."
mods = [
  "Correct Iron Warhammer",
  "Sword and ring of nerevar",
  "SM The Tools of Kagrenac",
  "Oriental Mesh Improvements",
  "Weapon Sheathing",
  "Glass Glowset and Weapon Sheathing Patch",
  "Lysol's Steel Claymore Replacer",
  "Remiros' Uniques",
  "Rubber's Weapons Pack",
  "Eltonbrand Replacer",
  "Improved Thrown Weapon Projectiles",
  "New Widowmaker",
  "New Bow of Shadows",
  "Animated Staff of Magnus",
  "Glowing Hasedoki Staff",
  "Skullcrusher Redone",
  "Daedric Crescent Replacer",
]

[[sublists]]
title = "Bodies And Heads"
description = "Replace the vanilla bodies and heads, and their textures, for all races and genders."
mods = [
  "Westly's Pluginless Head Replacer Complete",
  "Westly's Head and Hair Replacer - Hair Fix",
  "Westly's Faces Refurbished",
  "Westly's Faces Refurbished TR_Data patch",
  "Westlys Master Head Pack Prim and Proper",
  "Facelift for Tamriel Data",
  "Khajiit Head Pack",
  "Whiskers Patch for MMH Version - All NPCs and Base Replace",
  "Better Bodies 3.2 (Better Beasts)",
  "New Beast Bodies - Clean Version",
]

[[sublists]]
title = "Creature Visuals"
description = "Give the various creatures of the game new life with updated assets."
mods = [
  "Divine Dagoths",
  "Rotat's Corprus Natives Replacer",
  "Luminous Atronachs",
  "Westly Presents: Unique Winged Twilights",
  "HiRez Creatures - Netch",
  "Silt Strider",
  "Silt Strider recolor and clutter normalmaps for OpenMW",
  "Silt Strider Animation Restored",
  "Mudcrab Replacer",
  "Cliff Racer Replacer 2.0",
  "4thUnknowns Creatures Morrowind Edition",
  "Blighted Animals Retextured",
  "Blighted 4thUnknown's Beasts Retextured",
  "Alternate Spriggans",
  "Scamp Replacer",
  "Hunger Replacer",
  "Old Blue Fin",
  "SM Bonewalker Replacer",
  "SM BoneLord Replacer Overhaul",
  "On the Blink",
]

[[sublists]]
title = "VFX"
description = "Updated visual effects. Greatly enhance many scenes with subtle but beautiful additions."
mods = [
  "Mistify",
  "Remiros' Minor Retextures - Mist Retexture",
  "The Dream is the Door",
  "Parasol Particles",
  "Magic VFX Retexture by Articus",
  "Spells Reforged",
  "No Shield Sparkle",
  "Diverse Blood",
  "Improved Propylon Particles",
]

[[sublists]]
title = "Animations"
description = "Update the game's dated animations with improved assets."
mods = [
  "Simply Walking (Remastered)",
  "MCAR",
  "ReAnimation v2 - Rogue - first-person animation pack",
]

[[sublists]]
title = "Audio"
description = "Mods that add or change sounds or music."
mods = [
  "Same Low Price Fix",
  "MAO Spell Sounds",
]

[[sublists]]
title = "Exteriors"
description = "Updates of existing exterior locations, making them more varied by incoporating new assets and adding new points of interest."
mods = [
  "OAAB - Foyada Mamaea",
  "OAAB - The Ashen Divide",
  "Concept Art Molag Amur Region - The Great Scathes",
  "The Mountain of Fear",
  "Bal'laku - The Lonely Towers",
  "Gates of Ascadia",
  "Ascadia - Land of Pilgrimage",
  "The Great Seawall of Vivec",
  "Vivec Lighthouse",
  "Dagon Fel Lighthouse",
  "Little Landscapes - Bitter Coast Waterway",
  "Little Landscapes - Path to Pelagiad",
  "Compatible Odai River Upper Overhaul",
  "Little Landscapes - Path to Vivec Lighthouse",
  "Little Landscapes - Nix Hound Hunting Grounds",
  "Little Landscape - Path to Balmora",
  "Little Landscape - Foyada of Sharp Teeth",
  "The Grove of Ben'Abi",
  "The Grove of Ben'Abi Enhanced",
  "Telvanni Sea Beacons",
  "Marbled Zafirbel Bay",
  "Justice for Khartag (J.F.K.)",
  "Holamayan Island",
  "Sanctus Shrine",
  "Immersive Grotto Entrances",
]

[[sublists]]
title = "Groundcover"
description = "Add grass, reeds, small mushrooms, stones, and other groundcover to the game."
mods = [
  "Lush Synthesis",
  "Vurt's Groundcover",
  "Remiros' Groundcover",
  "Remiros Groundcover Textures Improvement",
  "OAAB Saplings",
  "Grass for Tamriel Rebuilt (Aesthesia or Remiros')",
]

[[sublists]]
title = "Cities & Towns"
description = "These make cities and settlements more beautiful by updating existing content or adding great new content of their own."
mods = [
  "Beautiful Cities of Morrowind",
  "Maar Gan - Town of Pilgrimage",
  "Nordic Dagon Fel",
  "Hanging Gardens of Suran",
  "Velothi Wall Art",
  "Concept Art Palace (Vivec City)",
  "Better Flames for Concept Art Palace (Vivec City) - OpenMW Version",
  "Guar Stables of Vivec",
  "Bell Towers of Vvardenfell",
  "Bell Towers of Vvardenfell Directional Sound for OpenMW",
  "Planters for Ajira",
  "RR Mod Series - Telvanni Lighthouse Tel Branora",
  "RR Mod Series - Telvanni Lighthouse Tel Vos",
  "Concept Arts plantations",
  "OAAB Grazelands",
  "OAAB Tel Mora",
  "Sadrith Mora BCOM Plus Module",
  "Caldera Governors Manor Redone",
  "Immersive Mournhold",
]

[[sublists]]
title = "Interiors"
description = "Updates of existing interior cells in order to make them feel more lived in and expansive."
mods = [
  "Almalexia's Chamber Overhaul",
  "Nordic Solstheim - Solstheim Interiors Overhaul",
  "Dagoth Ur Welcomes You",
  "Akulakhan's best chamber",
  "Azura's Shrine Overhaul",
  "Library of Vivec Enhanced",
  "Ald'ruhn-under-Skar",
  "Ald'Ruhn Manor Banners",
  "Rather Nice Factor's Estate",
  "Tamriel Rebuilt - Hall of Justice Overhaul",
]

[[sublists]]
title = "Caves & Dungeons"
description = "Mods that add, enhance, and/or replace various caves and/or dungeons."
mods = [
  "Mines and Caverns",
]

[[sublists]]
title = "NPCs"
description = "Add more variety and (hopefully) immersion by updating the rather limited NPC scope of the base game."
mods = [
  "NOD - NPC Outfit Diversity",
  "NOD - NPC Outfit Diversity Gigapixel Upscale",
  "Yet Another Guard Diversity",
  "Nordic Dagon Fel NPCs",
  "Better Almalexia",
  "VEHK - Concept art Vivec replacer",
  "By Azura",
  "Royal Barenziah",
  "Interesting Outfits - Solstheim",
  "Interesting Outfits - TR Imperial Guards",
]

[[sublists]]
title = "Dialogue"
description = "Adds new voiced lines."
mods = [
  "Dagoth Ur Voice addon v10",
  "Almalexia Voice",
  "Vivec Voice Addon Tribunal Version",
]

[[sublists]]
title = "Distant Details"
description = "Mods that affect object and visuals in the distance."
mods = [
  "Dynamic Distant Buildings for OpenMW",
  "Distant Ebon Tower for OpenMW",
]

[[sublists]]
title = "Mod Patches"
description = "Collections of small patches and other plugins for compatibility between various mods, or between various mods and OpenMW."
mods = [
  "MOMW Patches",
  "Sophie's Skooma Sweetshoppe",
  "Alvazir's Various Patches",
  "Khajiit Has Patches If You Have Coin",
  "Mono's Minor Moddities",
  "Publicola's Misc Mod Emporium",
  "Animated Morrowind and Weapon Sheathing patch for OpenMW",
  "Various tweaks and fixes",
]

[[sublists]]
title = "Post Processing Shaders"
description = "Post processing shaders add a variety of nice visual effects."
mods = [
  "MOMW Post Processing Pack",
]

[[sublists]]
title = "User Interface"
description = "Give the game UI a needed facelift in various ways."
mods = [
  "Monochrome User Interface",
  "Enchanting icons as you wish",
  "Gonzo's Splash Screens",
  "Alternative TrueType Fonts",
  "Big Icons",
  "Vanilla Style HD Icons for Attributes and Skills",
  "Vvardenfell Animated Main Menu",
  "Simple HUD for OpenMW (with compass or minimap)",
  "Small Skyrim Crosshair (OpenMW compatible)",
]

[[sublists]]
title = "Movies"
description = "Higher quality in-game cinematics."
mods = [
  "HD Intro Cinematic - English",
]

[[sublists]]
title = "Dev Build Only"
description = "Mods that require the latest OpenMW Developer Build."
mods = [
  "Dynamic Music (OpenMW Only)",
  "Dynamic Music - Muse Music Expansion Hlaalu Compatibility (OpenMW Only)",
  "Dynamic Music - Muse Music Expansion Ashlander Compatibility (OpenMW Only)",
  "Dynamic Music - Muse Music Expansion Redoran Compatibility (OpenMW Only)",
  "MUSE Music Expansion - Hlaalu",
  "MUSE Music Expansion - Ashlander",
  "MUSE Music Expansion - Redoran",
  "Tamriel Rebuilt - Original Soundtrack",
  "MUSE Music Expansion - Sixth House",
  "MUSE Music Expansion - Daedric",
  "MUSE Music Expansion - Dwemer",
  "MUSE Music Expansion - Tomb",
  "Vindsvept Solstheim for Dynamic Music (OpenMW)",
  "Harvest Lights",
  "Mage Robes for OpenMW-Lua",
  "HD Forge for OpenMW-Lua",
  "OpenMW Impact Effects",
  "OpenMW More Dynamic Water Meshes",
  "OpenMW (0.49) Distortion Effects",
  "OpenMW Distortion Effects Patches",
  "Animation Blending",
  "Camera Listener",
]

[[sublists]]
title = "Settings Tweaks"
description = "Not actually mods, but changes to settings that are built into the OpenMW engine. Enhance performance and visual quality, as well as enable or disable many gameplay options (including things provided by MCP for vanilla Morrowind)."
mods = [
  "True Nights and Darkness",
  "Distant Land And Objects",
  "Shadows",
  "Water",
  "Antialias Alpha Test",
  "NPCs Avoid Collisions",
  "Smooth Movement",
  "Swim Upward Correction",
  "Turn To Movement Direction",
  "Color Topic Enable",
  "MOMW Camera",
]

[[sublists]]
title = "Merging"
description = "The final bits of work, to ensure long-term smooth sailing."
mods = [
  "Delta Plugin and Groundcoverify",
  "S3LightFixes",
]
