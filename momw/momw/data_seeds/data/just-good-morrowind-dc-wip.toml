title = "Just Good Morrowind: Director's Cut WIP"
# Just Beautiful Morrowind?
description = """
<p>DUPE Limited but impactful graphical mods, performance optimizations, many gameplay mods, and no extra content or quests. Just Good Morrowind.</p><p>The vanilla Morrowind textures with normal maps look surprisingly nice for what they are. Combine that with the overwhelming beauty provided by various shaders and what you get is a simple yet enjoyable Morrowind experience, with gameplay changes and improvements to really spice things up. This mod list was optimized for maximum FPS on a Steam Deck.</p>

<p>This is not intended for a first time player – instead, check out <a href='{% url 'mod-list-detail' slug='i-heart-vanilla' %}'>I Heart Vanilla</a>.</p>"""
short_description = "Limited but impactful graphical mods, performance optimizations, many gameplay mods, and no extra content or quests. Just Good Morrowind."
wip = true

[[sublists]]
title = "First Steps"
description = "These are all the steps you need to follow before installing a mod list"
mods = [
  "Buy and Install Morrowind",
  "Install OpenMW",
  "Set your Site Settings",
  "Folder Deploy Script",
  "How to Install Mods",
]

[[sublists]]
title = "Patches"
description = "Alter some aspect of the game in order to fix a bug, or to be more consistent with the game itself. Patches usually work via a plugin."
mods = [
  "Patch for Purists",
  "Bethesda Official Plugins Naturalized",
]

[[sublists]]
title = "Consistency"
description = "Mods that fix inconsistencies in the Morrowind game world."
mods = [
  "Expansion Resource Conflicts",
]

[[sublists]]
title = "Modding Resources"
description = "Community asset repositories, used by many other mods."
mods = [
  "Tamriel Data",
]

[[sublists]]
title = "Meshes / Performance"
description = "Mesh replacers. Most of these correct mistakes in the originals, often resulting in performance increases, others will affect gameplay (Graphic Herbalism)."
mods = [
  "Morrowind Optimization Patch",
  "Graphic Herbalism - MWSE and OpenMW Edition",
  "Project Atlas",
  "Jammings Off",
  "Fixed Bonelord Arms",
]

[[sublists]]
title = "Land & Landmass Additions"
description = "Add large amounts of extra land, including quests, to the game."
mods = [
  "Tamriel Rebuilt",
  "Project Cyrodiil",
  "Skyrim: Home Of The Nords",
]

[[sublists]]
title = "Texture Packs"
description = "These provide large amounts of textures, covering all assets in the game or close to it."
mods = [
  "Morrowind Enhanced Textures",
  "Normal Maps for Everything",
]

[[sublists]]
title = "Interiors"
description = "Updates of existing interior cells in order to make them feel more lived in and expansive."
mods = [
  "Morrowind Interiors Project",
  "Kirel's Interior Weather",
]

[[sublists]]
title = "Sky & Weather"
description = "Alter the looks and behavior of the sky, clouds, and weather."
mods = [
  "Skies .IV",
  "Oh God Snow for Skies.iv and OpenMW",
  "New Starfields",
  "Meteors",
]

[[sublists]]
title = "Lighting"
description = "Update the behavior and look of lighting in the game, going for a darker and more accurate look overall."
mods = [
  "Glow in the Dahrk",
  "Nords shut your windows",
  "Improved Lights for All Shaders",
]

[[sublists]]
title = "Animations"
description = "Update the game's dated animations with improved assets."
mods = [
  "OpenMW Containers Animated",
  "Simply Walking (Remastered)",
  "Weapon Sheathing",
]

[[sublists]]
title = "Groundcover"
description = "Add grass, reeds, small mushrooms, stones, and other groundcover to the game."
mods = [
  "Lush Synthesis",
  "Vurt's Groundcover",
  "Remiros' Groundcover",
]

[[sublists]]
title = "NPCs"
description = "Add more variety and (hopefully) immersion by updating the rather limited NPC scope of the base game."
mods = [
  "Familiar Faces by Caleb",
]

[[sublists]]
title = "Companions"
description = "Adds companions to join you on your adventures."
mods = [
  "Attend Me",
]

[[sublists]]
title = "Balance And Nerfing"
description = "These mods try to bring some balance through changes to the game mechanics and world."
mods = [
  "Speechcraft Rebalance (OpenMW)",
  "Fatigue and Speed and Carryweight Rebalance (OpenMW)",
  "Pickpocket Rebalance (OpenMW)",
]

[[sublists]]
title = "Gameplay"
description = "Update gameplay mechanics. From leveling to monster spawning, marksman to sneaking, this selection tries to update game mechanics to feel better or more enjoyable compared to the vanilla game."
mods = [
  "At Home Alchemy - Finished",
  "NCGDMW Lua Edition",
  "Solthas Combat Pack (OpenMW Lua)",
  "Smart Ammo for OpenMW-Lua",
  "Zack's Lua Multimark Mod",
  "Pharis' Magicka Regeneration",
  "Light Hotkey",
  "Shield Unequipper",
  "NoPopUp Chargen",
]

[[sublists]]
title = "Quests"
description = "Add new quests, alter existing areas, get new rewards. Includes new content from some of the developers of the original Morrowind!"
mods = [
  "AFFresh",
  "Tamriel Rebuilt Introduction Quest - Help a Khajiit reach the City of Good People",
]

[[sublists]]
title = "Distant Details"
description = "Mods that affect object and visuals in the distance."
mods = [
  "Dynamic Distant Buildings for OpenMW",
]

[[sublists]]
title = "Mod Patches"
description = "Collections of small patches and other plugins for compatibility between various mods, or between various mods and OpenMW."
mods = [
  "Sophie's Skooma Sweetshoppe",
]

[[sublists]]
title = "Camera"
description = "Mods that affect the camera in one way or another."
mods = [
  "MOMW Camera",
  "Action Camera Swap",
]

[[sublists]]
title = "Post Processing Shaders"
description = "Post processing shaders add a variety of nice visual effects."
mods = [
  "MOMW Post Processing Pack",
]

[[sublists]]
title = "User Interface"
description = "Give the game UI a needed facelift in various ways."
mods = [
  "Alternative TrueType Fonts",
  "Big Icons",
  "Vanilla Style HD Icons for Attributes and Skills",
  "Cantons on the Global Map",
  "Pause Control",
  "Chocolate UI",
  "Enchanting icons as you wish",
  "Simple HUD for OpenMW (with compass or minimap)",
  "Small Skyrim Crosshair (OpenMW compatible)",
  "Loading Screens Diversified",
]

[[sublists]]
title = "Dev Build Only"
description = "Mods that require the latest OpenMW Developer Build."
mods = [
  "Perfect Placement",
  "Signpost Fast Travel",
  "Harvest Lights",
  "Convenient Thief Tools",
  "Go Home!",
  "Animation Blending",
  "Camera Listener",
]

[[sublists]]
title = "Settings Tweaks"
description = "Not actually mods, but changes to settings that are built into the OpenMW engine. Enhance performance and visual quality, as well as enable or disable many gameplay options (including things provided by MCP for vanilla Morrowind)."
mods = [
  "Antialias Alpha Test",
  "NPCs Avoid Collisions",
  "Smooth Movement",
  "Swim Upward Correction",
  "Turn To Movement Direction",
  "Color Topic Enable",
  "MOMW Gameplay",
]

[[sublists]]
title = "Merging"
description = "The final bits of work, to ensure long-term smooth sailing."
mods = [
  "Delta Plugin and Groundcoverify",
  "S3LightFixes",
]
