from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed
from .models import Mod


class LatestModsFeed(Feed):
    description = "Latest mod additions and updates on Modding-OpenMW.com"
    feed_type = Atom1Feed
    link = "/mods/"  # TODO: find out wtf this is for
    title = "Mod Feed"

    def items(self):
        return Mod.objects.all().order_by("-date_added")

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        return item.get_absolute_url()
