from pathlib import Path

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "momw_cache_table",
        "OPTIONS": {"CULL_FREQUENCY": 4, "MAX_ENTRIES": 1000},
    }
}
CACHE_MIDDLEWARE_SECONDS = 3600  # 1 hour
DEBUG = False
EMAIL_HOST = "localhost"
EMAIL_PORT = 25
INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.sitemaps",
    "chroniko",
    "media",
    "momw",
    "taggit",
    "utilz",
)
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": Path.home() / "logs" / "django.log",
        }
    },
    "loggers": {
        "django.request": {"handlers": ["file"], "level": "DEBUG", "propagate": True}
    },
}
MIDDLEWARE = []
