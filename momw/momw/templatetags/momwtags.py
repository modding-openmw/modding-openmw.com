import sys

from django import template
from django.utils.html import format_html
from ..models import Mod

register = template.Library()


@register.simple_tag
def mod_link(title):
    try:
        return format_html(
            "<a href='{}'>{}</a>", Mod.objects.get(name=title).get_absolute_url(), title
        )
    except Mod.DoesNotExist:
        print("ERROR")
        print(f"ERROR: The provided title does not match any mod: {title}")
        print("ERROR")
        sys.exit(1)


@register.simple_tag
def moved_mod_link(title, moved_to_slug_or_name):
    try:
        return format_html(
            "<a href='{}'>{}</a>",
            Mod.objects.get(slug=moved_to_slug_or_name).get_absolute_url(),
            title,
        )
    except Mod.DoesNotExist:
        try:
            return format_html(
                "<a href='{}'>{}</a>",
                Mod.objects.get(name=moved_to_slug_or_name).get_absolute_url(),
                title,
            )
        except Mod.DoesNotExist:
            print("ERROR")
            print(
                f"ERROR: The provided slug or name doesn't match any mod: {moved_to_slug_or_name}"
            )
            print("ERROR")
            sys.exit(1)


@register.simple_tag
def issue_link(number):
    return format_html(
        f"""<a href="https://gitlab.com/modding-openmw/modding-openmw.com/-/issues/{number}/">#{number}</a>"""
    )


@register.simple_tag
def moddinghall_mod(slug):
    return format_html(f"""https://mw.moddinghall.com/file/{slug}""")


@register.simple_tag
def moddinghall_user(username, display_name=None):
    if display_name is not None:
        return format_html(
            f"""<a href="<a href="https://mw.moddinghall.com/profile/{username}">{display_name}</a>"""
        )
    else:
        return format_html(
            f"""<a href="<a href="https://mw.moddinghall.com/profile/{username}">{username}</a>"""
        )


@register.simple_tag
def modhistory_archive_link(mod):
    return format_html(
        f"""https://web.archive.org/web/https://mw.modhistory.com/download-{mod}"""
    )


@register.simple_tag
def nexus_mod(modid):
    return format_html(f"""https://www.nexusmods.com/morrowind/mods/{modid}""")


@register.simple_tag
def nexus_user(username, display_name=None):
    if display_name is not None:
        return format_html(
            f"""<a href="https://next.nexusmods.com/profile/{username}/about-me">{display_name}</a>"""
        )
    else:
        return format_html(
            f"""<a href="https://next.nexusmods.com/profile/{username}/about-me">{username}</a>"""
        )
