from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from chroniko.models import BlogEntry
from media.models import MediaTrack
from .models import Category, Mod, ModList, Tag


class BlogEntrySitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return BlogEntry.live.all()

    def lastmod(self, obj):
        return obj.date_added


class CategorySitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.8

    def items(self):
        return Category.objects.all()


class MediaSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.6

    def items(self):
        return MediaTrack.live.all()

    def lastmod(self, obj):
        return obj.date_added


class ModSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        return Mod.objects.all()

    def lastmod(self, obj):
        return obj.date_added


class ModListSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        return ModList.objects.all()


class StaticPageSiteMap(Sitemap):
    changefreq = "weekly"
    priority = 0.7

    def items(self):
        return [
            "index",
            "about",
            "cfg_generator",
            "changelogs",
            "changelogs-website",
            "cookie",
            "faq",
            "faq-tooling",
            "mod_feed",
            "enable-in-cfg-generator-button",
            "getting_started",
            "gs-buy",
            "gs-install",
            "gs-settings",
            "gs-directories",
            "gs-tips",
            "guides",
            "guides-users",
            "guides-auto",
            "load_order",
            "mod-list-faq",
            "mod-lists",
            "mod_index",
            "mod_todo",
            "latest_mods",
            "mod_categories",
            "mod_tags",
            "privacy",
            "resources",
            "site-versions",
            "tips",
            "tips-cleaning",
            "tips-navmeshtool",
            "tips-performance",
            "tips-portable-install",
        ]

    def location(self, item):
        return reverse(item)


class TagSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.9

    def items(self):
        return Tag.objects.all()


sitemap_dict = {
    "mods": ModSitemap,
    "mod_lists": ModListSitemap,
    "mod_tags": TagSitemap,
    "mod_categories": CategorySitemap,
    "static_pages": StaticPageSiteMap,
}
