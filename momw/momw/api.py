from typing import List, Union
from ninja import NinjaAPI, Schema, ModelSchema

from .models import Action, Mod, ModArchive

import momw.views.dynamicpages as dpages
import momw.views.forms as forms

api = NinjaAPI()


class ActionSchema(ModelSchema):
    src: str = None
    dst: str = None
    path: str = None
    paths: List[str] = None
    force: bool = None
    arguments: Union[str, List[str]] = None

    class Meta:
        model = Action
        fields = ["action"]


class ModArchiveSchema(ModelSchema):
    actions: List[ActionSchema]
    file_name: str

    class Meta:
        model = ModArchive
        fields = ["direct_download", "extract_to", "nexus_file_id"]


class ModSchema(ModelSchema):
    usage_notes: str = None
    dir: str
    download_info: List[ModArchiveSchema]
    tags: List[str] = None
    on_lists: List[str] = None
    data_paths: List[str] = None

    class Meta:
        model = Mod
        fields = [
            "name",
            "author",
            "description",
            "url",
            "category",
            "dl_url",
            "compat",
            "slug",
            "date_added",
            "date_updated",
        ]


class OpenMWCfgSchema(Schema):
    fallback_values: str
    data_paths: List[str]
    fallback_archives: List[str]
    content_files: List[str]
    groundcover_files: List[str]

    class Config(Schema.Config):
        def underscores_to_spaces(x):
            return " ".join(x.split("_"))

        alias_generator = underscores_to_spaces
        populate_by_name = True


class CfgGeneratorSchema(Schema):
    settings_cfg: str
    openmw_cfg: OpenMWCfgSchema


@api.get("/lists", response=List[str], summary="Get all sluggified MOMW list names")
def lists(request):
    return dpages.mod_lists(request, True)


@api.get(
    "/lists/{slug}",
    response=List[ModSchema],
    summary="Get a list of all mods on a MOMW list",
)
def list_detail(request, slug: str):
    return dpages.mod_list_detail(request, slug, True)


@api.get(
    "/lists/{slug}/{step}",
    response=ModSchema,
    summary="Get a specifc mod description by the sluggified list name and step number",
)
def list_step_detail(request, slug: str, step: int):
    return dpages.mod_list_step_detail(request, slug, int(step), True)


@api.get(
    "/all",
    response=List[str],
    summary="Get all mod names that are present in the MOMW DB",
)
def all(request):
    return dpages.all_mods(request, True)


@api.get(
    "/mods/{slug}",
    response=ModSchema,
    summary="Get the details of a specific mod by its sluggified name",
)
def mod_detail(request, slug: str):
    return dpages.mod_detail(request, slug, True)


@api.get(
    "/cfg-generator/{preset}",
    response=CfgGeneratorSchema,
    by_alias=True,
    summary="Get config and settings information for a MOMW list by sluggified name",
)
def cfg_generator(request, preset: str):
    return forms.cfg_generator(request, preset, True)
