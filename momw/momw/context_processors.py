from django.conf import settings
from utilz.git import GIT_TAG


def momw_ver(request):
    return {"momw_rev": GIT_TAG, "momw_ver": settings.MOMW_VER}
