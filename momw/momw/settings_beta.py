from .settings_server import *  # NOQA

MOMW_VER = "beta"
PROJECT_HOSTNAME = "beta.modding-openmw.com"
SITE_NAME = "Modding-OpenMW.com"
USE_ROBOTS = True
SERVER_EMAIL = f"app@{SITE_NAME}"
