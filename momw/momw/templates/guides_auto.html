{% extends 'base.html' %}
{% block meta %}
  <meta property="og:site_name" content="Modding-OpenMW.com" />
  <meta property="og:type" content="website">
  <meta property="og:url" content="{% url 'guides-auto' %}{% if list_title and os %}/{{ selected_list }}/{{ os }}/{% endif %}">
  <meta property="og:title" content="Automatic Installation Guide{% if list_title and os %}: {{ list_title }} on {% if os == 'macos' %}macOS{% else %}{{ os|capfirst }}{% endif %}{% endif %}">
  <meta property="og:description" content="This page will walk you through each step required to automatically download and configure our mod lists.">
  <meta property="og:image" content="https://modding-openmw.com/favicon.png">
{% endblock %}
{% block title %}Automatic Installation Guide{% if list_title and os %}: {{ list_title }} on {% if os == 'macos' %}macOS{% else %}{{ os|capfirst }}{% endif %}{% endif %}{% endblock %}
{% block content %}
  <h1>Automatic Installation Guide</h1>

  <p>
    This page will walk you through each step required to automatically download and configure our mod lists.
  </p>

  <p>
    It's very important to fully complete each step and not skip anything.
  </p>

  <h3 id="choose-mod-list"><a href="#choose-mod-list">Choose A Mod List</a></h3>

  <p>
    Before getting started with our Automatic Installation Guide, you should first choose a mod list.
  </p>

  <p>
    The main mod lists and their descriptions are listed below. Click on the list title to view the complete list of mods used in each of the mod lists. We recommend you read through the mods in the mod list to get a feel for how the mod list will play.
  </p>

  <p>
    Click the bubble next to the one you wish to use and click the "Submit" button below to confirm your choice, and the page will update to match what you've selected:
  </p>

  <form action="{% url 'guides-auto' %}" method="get">
    <div class="radio-list">
      {% for list in mod_lists %}
        <div class="radio-list__item">
          <input required {% if selected_list == list.slug %}checked{% endif %} type="radio" id="{{ list.slug }}" name="selected-list" value="{{ list.slug }}" />
          <label for="{{ list.slug }}"><a class="bold" href="{{ list.get_absolute_url }}">{{ list.title }} ({{ list.mod_count }} mods)</a>: {{ list.short_description|safe }}</label>
        </div>
      {% endfor %}
    </div>

    <label for="os-select">Choose your operating system:</label>
    <select name="oses" id="os-select">
      <option {% if os == "windows" %}selected{% endif %} value="windows">Windows</option>
      <option {% if os == "linux" %}selected{% endif %} value="linux">Linux</option>
      <option {% if os == "macos" %}selected{% endif %} value="macos">macOS</option>
    </select>

    <div id="selected-mod-list">
      <button>Submit</button>
    </div>
  </form>

  {% if list_title %}
    <div class="line" style="margin-top: 14px;"></div>
    <h4>Selected mod list + OS: <span class="bold">{{ list_title }}</span> on <span class="bold">{% if os == 'macos' %}macOS{% else %}{{ os|capfirst }}{% endif %}</span></h4>
    <div class="line" style="margin-top: 14px;"></div>
    <div style="font-size: 1.4em;">
      <p>
        Thank you for selecting a mod list!
      </p>
      <p>
        Before continuing, please read <mark><a href="{% url 'mod-list-faq' %}">our mod list FAQ page</a></mark>, which answers many common questions regarding things that our mod lists add, change, and remove from the game. You're about to install potentially hundreds of different mods, so it's a good idea to read up on what you're in for.
      </p>
      <p>
        After you've read <mark><a href="{% url 'mod-list-faq' %}">our mod list FAQ page</a></mark>, please carefully read each section below to learn how to complete the rest of the install and config process.
      </p>
      <p>
        Don't skip any steps, or you will end up with a broken, incomplete setup.
      </p>
    </div>
  {% else %}
    <div class="bold" style="font-size: 1.4em;">
      <p>
        Before you can continue you must select a mod list above.
      </p>
      <p>
        To do so: click the "submit" button above after you've clicked the bubble button next to the mod list you wish to install and selected your operating system.
      </p>
    </div>
  {% endif %}

  {% if list_title and os %}
  <div class="line" style="margin-top: 14px;"></div>

  <h2>Table of contents</h2>

  <ol>
    <li><a href="#install-openmw">Install OpenMW</a></li>
    <li><a href="#momw-tools-pack">Install the MOMW Tools Pack</a></li>
   	<li><a href="#umo">Install A Mod List With umo</a></li>
	<li><a href="#configurator">Run the MOMW Configurator</a></li>
	<li><a href="#gameplay-settings">Gameplay Settings and Others</a></li>
    <li><a href="#run-the-game">Run The Game</a></li>
    <li><a href="#read-the-faq">Read The FAQ</a></li>
  </ol>

  {% if os == "linux" %}
    <p><iframe width="640" height="360" src="https://www.youtube.com/embed/EfC-5D5tL88" title="MOMW Tools Pack (Linux UMO Guide)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe></p>
  {% else %}
    <p><iframe width="640" height="360" src="https://www.youtube.com/embed/wDa_q4djhzw" title="Modding-OpenMW.com Automatic Modlist Installation Guide" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe></p>
  {% endif %}

  <h3 id="install-openmw"><a href="#install-openmw">Install OpenMW</a></h3>

  <p>
    One must of course <a href="{% url 'gs-buy' %}">buy a copy of Morrowind</a> and <a href="{% url 'gs-install' %}">install OpenMW</a>.
  </p>

  <blockquote>
    Please note that most mods, and by proxy our mod lists, are only compatible with the English language version of Morrowind.
  </blockquote>

  <p>
    Once you have Morrowind installed, you will need to decide whether you want to install the latest stable version of OpenMW or the latest development build.
  </p>

  <p>
    The development build may have bugs and/or untested features. If you decide to use the development build, however, you can opt-in to several mods that take advantage of the latest features in development.
  </p>

  <p>
    You can find the list of mods that require the development build <a href="{% url 'mod_tag_detail' slug='dev-build-only' %}">here</a>.
  </p>

  {% if os == "windows" %}
  <p class='bold'>OpenMW Stable installer instructions:</p>
  <ul>
    <li><a href='https://openmw.org/downloads'>Download the installer</a> for the latest stable release</li>
    <li>Run the installer. It will have you choose the location where you would like to install OpenMW</li>
  </ul>
  <p class='bold'>OpenMW Development Build installation instructions:</p>
  <ul>
    <li><a href='https://openmw.org/downloads'>Download the latest development build</a></li>
    <li>Note: This requires the <code>Visual C++ redistributable</code> to be installed, and a link is provided underneath <a href='https://openmw.org/downloads'>the download link</a></li>
    <li>Extract the contents of the <code>.zip</code> archive to the location where you would like to install OpenMW</li>
  </ul>

  <p>
    When you have OpenMW installed, you may proceed to <a href="#momw-tools-pack">the next section</a>.
  </p>

  {% elif os == "linux" %}
  <p class='bold'>OpenMW Stable installer instructions:</p>
  <ul>
    <li>Download the release tarball from <a href="https://downloads.openmw.org/linux/generic/?C=M&O=D">here</a>
      <ul>
        <li>Alternatively, a release AppImage can be found <a href="https://modding-openmw.com/mods/openmw-appimage/#usage-info">here - download the Engine (or Launcher if desired), INI Importer, and Navmeshtool AppImages</a></li>
        <li><span class="bold">Note: Flatpak is not supported by this guide! Please don't use that version.</span></li>
      </ul>
    </li>
    <li>Extract the tarball or place the AppImage into a directory of your choosing (for example <code>$HOME/games/OpenMW</code>)</li>
  </ul>
  <p class='bold'>OpenMW Development Build installation instructions:</p>
  <ul>
    <li>Download a dev build tarball from <a href="https://redfortune.de/openmw/nightly/?C=M;O=D">here</a>
      <ul>
        <li>Alternatively, a dev build AppImage can be found <a href="https://modding-openmw.com/mods/openmw-appimage/#usage-info">here - download the Engine (or Launcher if desired), INI Importer, and Navmeshtool AppImages</a></li>
        <li><span class="bold">Note: Flatpak is not supported by this guide! Please don't use that version.</span></li>
      </ul>
    </li>
    <li>Extract the tarball or place the AppImage into a directory of your choosing (for example <code>$HOME/games/OpenMW</code>)</li>
  </ul>

  <p>
    When you have OpenMW installed, you may proceed to <a href="#momw-tools-pack">the next section</a>.
  </p>

  {% elif os == "macos" %}
  <p class='bold'>OpenMW Stable installer instructions:</p>
  <ul>
    <li>Download the release .dmg from <a href="https://downloads.openmw.org/osx/?C=M&O=D">here</a></li>
    <li>Mount the .dmg file</li>
    <li>Drag OpenMW and/or OpenMW-CS into Applications folder</li>
    <li>Try to run OpenMW, click "Done" when you are blocked</li>
    <li>Open system settings, go to Privacy and Security, click "Open Anyway" under OpenMW (you have to click "Open Anyway" twice)</li>
  </ul>
  <p class='bold'>OpenMW Development Build installation instructions:</p>
  <ul>
    <li>Download a dev build .dmg from <a href="https://openmw.org/downloads/">here (ARM only)</a></li>
    <li>Mount the .dmg file</li>
    <li>Drag OpenMW and/or OpenMW-CS into Applications folder</li>
    <li>Try to run OpenMW, click "Done" when you are blocked</li>
    <li>Open system settings, go to Privacy and Security, click "Open Anyway" under OpenMW (you have to click "Open Anyway" twice)</li>
  </ul>

  <p>
    When you have OpenMW installed, you may proceed to <a href="#momw-tools-pack">the next section</a>.
  </p>
  {% endif %}

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="momw-tools-pack"><a href="#momw-tools-pack">Install MOMW Tools Pack</a></h3>

  <p>
    The <a href="https://modding-openmw.gitlab.io/momw-tools-pack/">MOMW Tools Pack</a> contains all of the tools you need to install our mod lists in one download. To install it:
  </p>

  <ol>
    <li>Download the pack for your operating system from <a href="https://modding-openmw.gitlab.io/momw-tools-pack/">this link</a></li>
    <li>Extract it to a folder on your computer, for example: {% if os == "windows" %}<code>C:\games\momw-tools-pack-windows</code>{% elif os == "linux" %}<code>/home/username/games/momw-tools-pack-linux</code>{% elif os == "macos" %}<code>/Users/username/games/momw-tools-pack-macos</code>{% endif %}</li>
  </ol>

  <p>That's it! You're ready to begin installing mods.</p>

  {% if os == "windows" %}
  <p>Note: The umo and groundcoverify executables might get flagged as malware by your AV. These are false positives. Check <a href="https://gitlab.com/modding-openmw/umo/-/issues/49">this issue</a> on the umo gitlab page for an explanation about why this happens.</p>
  {% endif %}

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="umo"><a href="#umo">Install A Mod List With umo</a></h3>
  <p>
    You will now use umo (included in the Tools Pack) to download, extract, and prepare all mods for your mod list:
  </p>

  {% if os == "windows" %}
  <ol>
    <li>Download and install <a href="https://www.7-zip.org/">7-zip</a> if you do not already have it</li>
	<li>Go to <a href='https://www.nexusmods.com/'>Nexus Mods</a> and log into your Nexus account, or create one if you need to.</li>
	<li>Open the folder you extracted the MOMW Tools Pack to in Explorer</li>
    <li>Shift+Right-Click the folder background to open the context menu, and select 'Open Powershell window here'</li>
	<li>With the Powershell window in focus copy-paste the following commands <span class="bold">one at a time</span> and press enter to run each one:
      <pre><code># First, run and complete setup
.\umo.exe setup

# Next, sync data
.\umo.exe cache sync {{ selected_list }}

# Finally, install and extract mods:
.\umo.exe install {{ selected_list }}</code></pre>
    </li>
	<li>Please note that the umo setup process will ask you for various executables, and you must also include the <code>.exe</code> part in the path you give it, for example:
      <pre><code># Correct:
C:\games\MOMWToolsPack\tes3cmd.exe

# Incorrect:
C:\games\MOMWToolsPack\tes3cmd</code></pre>
    </li>
  </ol>

  <p>
    When umo has finished, you may proceed to <a href="#configurator">the next section</a>.
  </p>

  {% elif os == "linux" %}
  <ol>
    <li>Install <code>p7zip</code> using your system package manager
      <ul>
        <li>Void Linux users: be sure to also install <code>7zip-unrar</code> and select it as your <code>rar</code> program of choice:
          <pre><code>sudo xbps-install -S 7zip-unrar
sudo xbps-alternatives -s 7zip-unrar</code></pre>
        </li>
      </ul>
    </li>
	<li>Go to <a href='https://www.nexusmods.com/'>Nexus Mods</a> and log into your Nexus account, or create one if you need to.</li>
    <li>Open a terminal</li>
    <li><code>cd</code> to the directory where you extracted the MOMW Tools Pack to in your file manager</li>
	<li>With the terminal window in focus, copy+paste the following commands <span class="bold">one at a time</span> and press Enter to run each one:
      <pre><code># First, run and complete setup
./umo setup

# Next, sync data
./umo cache sync {{ selected_list }}

# Finally, install and extract mods:
./umo install {{ selected_list }}</code></pre>
    </li>
  </ol>

  <p>
    When umo has finished, you may proceed to <a href="#configurator">the next section</a>.
  </p>

  {% elif os == "macos" %}
  <ol>
    <li>Install <code>7-Zip</code> and <code>unar</code> via Homebrew:
      <pre><code>brew install sevenzip unar</code></pre>
      <ul>
        <li>Please see <a href="https://docs.brew.sh/Installation">this guide</a> for more information about how to install Homebrew.</li>
      </ul>
    </li>
	<li>Go to <a href='https://www.nexusmods.com/'>Nexus Mods</a> and log into your Nexus account, or create one if you need to.</li>
    <li>Open the folder you extracted the MOMW Tools Pack to in Finder</li>
    <li>Drag <code>umo.app</code> into <code>/Applications</code></li>
	<li>Open a terminal and <code>cd</code> into the folder you extracted the MOMW Tools Pack to and copy+paste the following commands <span class="bold">one at a time</span> and press Enter to run each one:
      <pre><code># First, run and complete setup
./umo setup

# Next, sync data
./umo cache sync {{ selected_list }}

# Finally, install and extract mods:
./umo install {{ selected_list }}</code></pre>
    </li>
    <li>Please note: after running <code>umo setup</code> you may need to run the <code>umo.app</code> that is put in the dist folder for the protocol handler to be registered.</li>
  </ol>

  <p>
    When umo has finished, you may proceed to <a href="#configurator">the next section</a>.
  </p>
  {% endif %}

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="configurator"><a href="#configurator">Run the Configurator</a></h3>

  <p>
    You will now use MOMW Configurator to install a full, correct load order as well as generate merged plugins and a navmesh DB cache.
  </p>

  <p>
    <span class="bold">Important: Do not run OpenMW-Launcher and then enable every plugin you have downloaded. It is totally normal to have many unchecked plugins in the content files tab. Many mods come with alternate versions and patches that are incompatible with each other and will cause your game to break.</span>
  </p>

  <p>
    Using the MOMW Configurator enables all required plugins and data paths, and in the correct load order. <span class="bold">You should not re-order anything or enable anything extra.</span>
  </p>

  {% if os == "windows" %}
  <ol>
	<li>Open the folder you extracted the MOMW Tools Pack to in Explorer</li>
    <li>Shift+Right-Click the folder background to open the context menu, and select 'Open Powershell window here'</li>
	<li>With the Powershell window in focus, copy-paste one of the following commands and press enter to run it:
      <pre><code># If you are using OpenMW 0.48 (Release):
.\momw-configurator.exe config {{ selected_list }} --run-navmeshtool --run-validator --verbose

# If you are using an OpenMW 0.49 dev build:
.\momw-configurator.exe config {{ selected_list }} --run-navmeshtool --run-validator --verbose --dev</code></pre>
    </li>
    <li>You will be prompted to enter the location of the <code>openmw-iniimporter.exe</code> file
      <ul>
        <li>This will be in the folder where you installed OpenMW</li>
      </ul>
    </li>
    <li>When the Configurator finishes, you're almost ready to run OpenMW! Please proceed to <a href="#gameplay-settings">the next section</a>.</li>
  </ol>

  {% elif os == "linux" %}
  <p class="bold">Reminder: The Flatpak version of OpenMW is not supported!</p>
  <ol>
	<li>Open a terminal window</li>
    <li><code>cd</code> to the directory where you extracted the MOMW Tools Pack to in your file manager</li>
	<li>With the terminal window in focus copy-paste one of the following commands and press enter to run it:
      <pre><code># If you are using OpenMW 0.48 (Release):
./momw-configurator-linux-amd64 config {{ selected_list }} --run-navmeshtool --run-validator --verbose

# If you are using an OpenMW 0.49 dev build:
./momw-configurator-linux-amd64 config {{ selected_list }} --run-navmeshtool --run-validator --verbose --dev</code></pre>
    </li>
    <li>You will be prompted to enter the location of the <code>openmw-iniimporter</code> file
      <ul>
        <li>If you installed AppImages, this will be in the folder where you installed OpenMW</li>
        <li>If you installed OpenMW from your system package manager, it will likely be in PATH and found automatically (if not, locate it with the command: <code>which openmw-iniimporter</code>)</li>
      </ul>
    </li>
    <li>When the Configurator finishes, you're almost ready to run OpenMW! Please proceed to <a href="#gameplay-settings">the next section</a>.</li>
  </ol>

  {% elif os == "macos" %}
  <ol>
	<li>Open a terminal window</li>
    <li><code>cd</code> to the directory where you extracted the MOMW Tools Pack to in your file manager</li>
	<li>With the terminal window in focus copy-paste one of the following commands and press enter to run it:
      <pre><code># If you are using OpenMW 0.48 (Release):
./momw-configurator-macos-arm64 config {{ selected_list }} --run-navmeshtool --run-validator --verbose

# If you are using an OpenMW 0.49 dev build:
./momw-configurator-macos-arm64 config {{ selected_list }} --run-navmeshtool --run-validator --verbose --dev</code></pre>
    </li>
    <li>Please note: if you're using an x86 mac then your executable will be named <code>momw-configurator-macos-amd64</code></li>
    <li>You will be prompted to enter the location of the <code>openmw-iniimporter</code> file
      <ul>
        <li>This will be located in your <code>/Applications/OpenMW.app/Contents/MacOS</code> directory</li>
      </ul>
    </li>
    <li>When the Configurator finishes, you're almost ready to run OpenMW! Please proceed to <a href="#gameplay-settings">the next section</a>.</li>
  </ol>
  {% endif %}

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="gameplay-settings"><a href="#gameplay-settings">Gameplay Settings and Others</a></h3>

  <p>
    The Configurator ensures some settings for the <code>settings.cfg</code> file that are either required or heavily recommended to play the modlist. However, OpenMW allows for plenty of user customization and includes dozens of options, many of which are highly subjective, so it's hard to give a rundown of what you should or shouldn't use.
  </p>

  <p><span class="bold">It is vitally important that you DO NOT edit the <code>settings.cfg</code> file while the launcher is open, and also DO NOT open the launcher while the <code>settings.cfg</code> file is still open in a text editor!</span></p>

  <p>
    We recommend hovering over each option in the Settings menu of the OpenMW-Launcher to view the tooltip and decide for yourself which options you want to toggle. You should also read <a href='https://openmw.readthedocs.io/en/latest/reference/modding/settings/game.html'>the official documentation</a> for the rest of the settings to get a more complete idea of what can be done. You can also visit our <a href="{% url 'gs-settings' %}">Getting Started: Tweak Settings</a> page, which highlights some options.
  </p>

  <p>
    You can always re-run the Configurator to reapply any settings values we put there for you.
  </p>

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="run-the-game"><a href="#run-the-game">Run The Game</a></h3>

  <p>
    Last, but not least: run the game to see how it looks and all that.
  </p>

  <p>
    OpenMW has a handy testing feature that's described <a href="{% url 'tips-performance' %}#how-to-test">here</a>. Be sure to use that to quickly spawn into various areas as desired – maybe there's a texture you still want to replace or a problem you found (please <a href="https://gitlab.com/modding-openmw/modding-openmw.com/-/issues">let us know</a> if that's the case!). Otherwise, it is definitely worthwhile to spend time testing before you take the deep plunge.
  </p>

  <p>
    Once you're in-game with the testing feature described above, it's a good idea to check out the various settings related to detail levels. In particular, you'll want to extend the "View Distance" setting found at: ESC >> Options >> click the Video tab >> click the Detail Level tab below that >> adjust the View Distance slider. Please keep in mind that a larger view distance will have a larger impact on performance, so try different values until you find something that works for you.
  </p>

  <p>
    This is also a good time to nail down performance and quality settings. With a reasonably modern GPU, it should be possible to run Morrowind via OpenMW with a high amount of visual detail. See the entire <a href="{% url 'tips-performance' %}">performance guide</a> for more information.
  </p>

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="read-the-faq"><a href="#read-the-faq">Read The FAQ</a></h3>
  <p>
    Stop right there, criminal scum! You thought you could get away with not reading the FAQ before, but here we are again. Seriously, many of the questions you will have about the potentially hundreds of mods you've installed will be answered in the <a href="{% url 'mod-list-faq' %}">Mod List FAQ</a>, so please read it. We spend a lot of time solving technical issues and maintaining the mod lists, so we would really appreciate it if you took the time to at least search the <a href="{% url 'mod-list-faq' %}">Mod List FAQ</a> for the answer to your question.
  </p>

  <div class="line" style="margin-top: 14px;"></div>

  <h3 id="update-warning"><a href="#update-warning">A Warning About Updating Mods</a></h3>

  <p>
    Before you begin playing the game, it's worth issuing a warning about updating mods mid-playthrough:
  </p>

  <p class="bold">
    It is not advised to update mods if you already have an in-progress savegame and are mid-playthrough!
  </p>

  <p>
    Some mods may be safe to update mid-playthrough, but there's no hard rule, so you must be careful. To be on the safe side, please avoid doing a mass update with umo and only update as needed to fix critical bugs in specific mods if there are any.
  </p>

  <p>
    If there is a need to update an individual mod for such a bug fix, you can do it with umo like this:
  </p>

  <pre><code># Windows
.\umo.exe cache sync {{ selected_list }} --skip-momw --subset ProjectAtlas
.\umo.exe install {{ selected_list }} --subset ProjectAtlas

# Linux/macOS
./umo cache sync {{ selected_list }} --skip-momw --subset ProjectAtlas
./umo install {{ selected_list }} --subset ProjectAtlas</code></pre>

  <p>
    The above example would update <span class="bold">just</span> Project Atlas without touching anything else.
  </p>

  <p>
    <span class="bold">Always be extra sure it's safe to do so before updating any mod mid-playthrough!</span>
  </p>

  <div class="line" style="margin-top: 14px;"></div>

  <p style="font-size: 1.8em;">
    Hey, that's it!
  </p>

  <p style="font-size: 1.8em;">
    Congratulations: if you made it this far and completed every step, then you're ready to play the game. Have fun!
  </p>

  {% endif %}

{% endblock %}
