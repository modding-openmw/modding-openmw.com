{% extends 'base.html' %}
{% block title %}Tips: Customizing Mod Lists{% endblock %}
{% block content %}
  <h3 class="center">Tips: <span class="bold">Customizing Mod Lists</span></h3>

  <div class="bold" style="font-size: 1.45em;">
    <p class="center">!!!! WARNING !!!!</p>
    <p>
      The mod lists on this website are carefully assembled with many patches to ensure things work together. You should only add or remove mods from one of our lists if you know what you're doing and you are reasonably sure that you won't create any conflicts.
    </p>
    <p>
      Mod list customizations are NOT supported by the MOMW team, you're totally on your own when you customize a list!
    </p>
  </div>

  <h4>Prerequisites</h4>
  <p>
    The instructions on this page assume you've followed our <a href="{% url 'guides-auto' %}">Automatic Installation Guide</a> and successfully installed at least one mod list that way.
  </p>
  <p class="bold" style="font-size: 1.2em;">
    We do not recommend using a load order sorting tool such as PLOX to sort your customized mod list. Such tooling is known to break our carefully-sorted load order and should not be used!
  </p>

  <h4>Installing custom mods with umo</h4>
  <p>
    The umo mod installation tool supports two ways of installing mods that aren't on a mod list:
  </p>
  <ol>
    <li>Mods not found on nexusmods.com (or mods that are not primarily linked to on Nexus) but listed on this website can be added to umo's "custom" list by clicking the "Install with umo" button which can be found on mod detail pages for qualifying mods, for example <a href="{% url 'mod_detail' slug='abandoned-flat-v2' %}">Abandoned Flat V2</a>
      <ul>
        <li>Mods installed this way will be placed into the <code>custom</code> directory under your mod base directory. For example, Abandoned Flat V2 would be under <code>mod_base_dir/custom/PlayerHomes/AbandonedFlatV2</code></li>
      </ul>
    </li>
    <li>Mods found on nexusmods.com can be added to umo's "custom" list by clicking on the "Mod Manager Download" button for the mod file you wish to add. For example, <a href="https://www.nexusmods.com/morrowind/mods/55396/">(OpenMW) Hit and Miss Percentage Indicators for Combat</a> - click the "Files" tab, then click on the "Mod Manager Download" button. That will open a new page, from there click "Download" to add the mod to umo's custom list.
      <ul>
	    <li>Once you've used the "Mod Manager Download" button, open the terminal of your choice in your tools folder, for example Powershell.</li>
		<li>Run <code>./umo cache sync custom</code> followed by <code>./umo install custom</code>. This will download and extract the mods.</li>
        <li>Mods installed this way will be placed into the <code>custom/umo</code> directory under your base mods directory, they will not be under a category subdirectory as non-Nexus mods are.</li>
      </ul>
    </li>
  </ol>

  <h4>Adding custom mods to your load order with MOMW Configurator</h4>
  <p>
    The MOMW Configurator tool allows you to define mod list customizations in a configuration file, allowing for repeatable changes to our mod lists in the form of:
  </p>
  <ul>
    <li>Insertions</li>
    <li>Replacements</li>
    <li>Removals</li>
  </ul>

  <p>
    These rules should be defined in a file called <code>momw-customizations.toml</code> in the same folder as your <code>openmw.cfg</code> and <code>momw-configurator.toml</code> files. If that file does not already exist, simply create it.
  </p>

  <p>
    <a href="https://modding-openmw.gitlab.io/momw-configurator/#customize-the-openmw.cfg-file">The MOMW Configurator README</a> fully describes all customization options, but we'll show a few examples below.
  </p>

  <h5>Insertions</h5>

  <p>
    You can insert a single line like this:
  </p>

  <pre><code>[[Customizations]]
listName = "just-good-morrowind"
[[Customizations.insert]]
insert = "C:\\games\\OpenMWMods\\Gameplay\\GoHome"
after = "AttendMe/AttendMe_1.8"</code></pre>

  <p>
    Or a block of lines like this:
  </p>

  <pre><code>[[Customizations]]
listName = "just-good-morrowind"
[[Customizations.insert]]
insertBlock = """
go-home.omwscripts
go-home.omwaddon
"""
before = "AttendMe.omwscripts"</code></pre>

  <p>
    The usage of triple quotes (<code>"""</code>) is important when doing block insertions!
  </p>
  
  <h5>Replacements</h5>

  <p>
    You can replace lines like this:
  </p>

  <pre><code>[[Customizations.replace]]
source = "MonochromeUserInterface"
dest = "C:\\games\\OpenMWMods\\UserInterface\\ChocolateUI"</code></pre>

  <h5>Removals</h5>

  <p>
    To remove the Bethesda and intro videos (the ones that play before the main menu) from Total Overhaul, add the following to your <code>momw-customizations.toml</code> file:
  </p>

  <pre><code>[[Customizations]]
listName = "total-overhaul"
removeFallback = [
  "Movies_Company_Logo,bethesda logo.bik",
  "Movies_Morrowind_Logo,mw_logo.bik",
]</code></pre>

  <h5>Apply Your Customizations</h5>

  <p>
    Once you've written customization rules into your <code>momw-customizations.toml</code> file, you can now apply them by running the Configurator as normal:
  </p>

  <pre><code>momw-configurator config i-heart-vanilla
2024/11/16 18:55:51 Welcome to MOMW Configurator v1.8
2024/11/16 18:55:51 Configurating mod list: i-heart-vanilla
2024/11/16 18:55:51 Fetching mod list configs
2024/11/16 18:55:51 Running: openmw-iniimporter
2024/11/16 18:55:51 Writing mod list load order to openmw.cfg
2024/11/16 18:55:51 Applying customizations
2024/11/16 18:55:51 Ensuring recommended values are set in your settings.cfg file
2024/11/16 18:55:51 MOMW Configurator completed: i-heart-vanilla</code></pre>

  <p>
    The <code>Applying customizations</code> part of the log output indicates that your customizations are being used. You can always skip that by using the <code>--no-customizations</code> argument.
  </p>
  
  <h4>Provided Example: Kezyma's Voices of Vvardenfell</h4>
  
  <p>
    Kezyma's Voices of Vvardenfell is one of the most popular additions people want to make to their mod list, so let's use it as an example to bring it all together. Keep in mind that Voices of Vvardenfell requires OpenMW 0.49 or newer. You'll need to download 3 mods for this. Again, use the "Mod Manager Download" button, followed by the "Slow Download" button for each of these mods to add them to umo's custom list. Alternatively, you could use the "Download with umo" button on each of these mod's pages on this website, but for sake of example we'll download these from Nexus.
  </p>
  <ul>
    <li><a href='https://www.nexusmods.com/morrowind/mods/52279'>Kezyma's Voices of Vvardenfell</a></li>
	<li><a href='https://www.nexusmods.com/morrowind/mods/54629'>OpenMW Lua helper utility</a></li>
	<li><a href='https://www.nexusmods.com/morrowind/mods/54137'>OpenMW Voices of Vvardenfell patch</a></li>
  </ul>
  <p>
    Once you've used the "Mod Manager Download" buttons on each of these pages, open your terminal of choice inside your tools folder, for example Powershell, and run the following two commands:
	<ul>
	  <li><code>./umo cache sync custom</code></li>
	  <li><code>./umo install custom</code></li>
    </ul>
	This will download and extract those three mods and place them into the <code>custom/umo</code> directory in your base mods folder.
  </p>
  <p>
    Now, edit your <code>momw-customizations.toml</code> file. Again, this is normally found next to your <code>openmw.cfg</code> and <code>momw-configurator.toml</code> files, but you may need to create it if you don't have one already present. Once you have this file open in your favorite text editor insert the following lines, or just the lines you need to fill it in if some lines are already there. It's important to note that I'm using the total-overhaul mod list on Windows and my mods folder is <code>F:\Mods\custom\umo</code>, so make sure to change those according to your own situation.
  </p>
  <pre><code>[[Customizations]]
listname = "total-overhaul"
[[Customizations.insert]]
insert = "F:\\Mods\\custom\\umo\\KezymasVoicesofVvardenfell\\VoicesofVvardenfell023\\00 - Core"
after = "PatchforPurists"
[[Customizations.insert]]
insert = "F:\\Mods\\custom\\umo\\KezymasVoicesofVvardenfell\\VoicesofVvardenfell023\\01 - Patch for Purists"
after = "KezymasVoicesofVvardenfell\\00 - Core"
[[Customizations.insert]]
insert = "F:\\Mods\\custom\\umo\\OpenMWLuahelperutility\\OpenMWLuahelperutilityv051"
after = "KezymasVoicesofVvardenfell\\01 - Patch for Purists"
[[Customizations.insert]]
insert = "F:\\Mods\\custom\\umo\\OpenMWVoicesofVvardenfellpatch\\OpenMWVoicesofVvardenfellpatchAllDialogue"
after = "OpenMWLuaHelper"
[[Customizations.insert]]
insert = "OpenMW_luahelper_dialog.esp"
after = "Bloodmoon.esm"
[[Customizations.insert]]
insert = "OpenMW_luahelper_dialog_TR_2412patch.esp"
after = "TR_Mainland.esm"
[[Customizations.insert]]
insert = "OpenMW_luahelper.esp"
after = "Patch for Purists - Semi-Purist Fixes.ESP"
[[Customizations.insert]]
insert = "OpenMW_luahelper.omwscripts"
after = "OpenMW_luahelper.esp"
[[Customizations.insert]]
insert = "VoV OpenMW.omwscripts"
after = "OpenMW_luahelper.omwscripts"</code></pre>
  <p>
    To explain again what's happening here, you start with the <code>[[Customizations]]</code> header, and then provide the mod list name of the mod list you're wanting to customize, and then a series of <code>[[Customization.insert]]</code> blocks for each required data path and plugin, and where you want them to be put. Note that for Windows users paths contain backslashes <code>\</code>, which need to be doubled <code>\\</code> in-between double-quotes in order to be counted since this is a <code>.toml</code> file.
  </p>
  <p>
    After you save this file, open your terminal of choice inside your tools folder again, for example Powershell, and run the configurator. Again, I'm using the total-overhaul mod list on Windows, so change the appropriate bits to match your own situation.
	<ul>
	  <li><code>./momw-configurator config -v -d --run-validator total-overhaul</code></li>
	</ul>
  </p>
  <p>
    That's it. Once the configurator finishes configurating your customizations will be applied if you've done them correctly, and you're ready to test it out.
  </p>

  <h5>Things To Remember</h5>

  <ul>
    <li>Customizations for each mod list should go under a top-level <code>[[Customizations]]</code> section, a <code>listName</code> definition is required under each such top-level section</li>
    <li>You should not include prefixes such as <code>fallback=</code>, <code>data=</code>, or <code>content=</code> when specifying lines to customize</li>
    <li>When specifying a line to insert before or after, or a source line to replace, you need not list the entire line but you will need to add enough of it such that it can be found (in the case of lines that contain similar text strings e.g., <code>00 Core</code>)</li>
    <li>If in doubt about a feature or how to do something, please consult <a href="https://modding-openmw.gitlab.io/momw-configurator/#customize-the-openmw.cfg-file">The MOMW Configurator README</a>. If that still doesn't explain things or you still have questions, please ask for help in <a href="https://discord.gg/KYKEUxFUsZ">our Discord channel on the OpenMW server</a>. We can only help with the overall customization workflow, <span class="bold">we cannot help with ensuring your customizations are valid and don't cause conflicts or other problems</span>.</li>
  </ul>
  
{% endblock %}
