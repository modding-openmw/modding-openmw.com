{% extends 'base.html' %}
{% block title %}Tips: Custom Shaders{% endblock %}
{% block content %}

  <h3 class="center">Tips: <span class="bold">Custom Shaders</span></h3>

  {% include "tips-nav.html" with cur_page="tips-custom-shaders" %}

  <p>
    Most users will simply use shader mods such as the ones included in <a href="/mods/momw-post-processing-pack/">MOMW Post Processing Pack</a>, but in some circumstances you may want to use custom shaders with OpenMW. What is the benefit of doing this? They have the potential to adjust how the game looks in various ways, sometimes with great effect.
  </p>

  <h4>Table Of Contents</h4>

  <ul>
    <li><a href="#installing">Installing Custom Shaders</a></li>
    <li><a href="#vtasteks-shaders">Vtastek's Shaders</a></li>
    <li><a href="#lightfixes-setup">Generating <code>LightFixes.esp</code>: Setup</a></li>
    <li><a href="#lightfixes-plugin">Generating <code>LightFixes.esp</code>: Get The Plugin</a></li>
    <li><a href="#managing-resources">Managing OpenMW's <code>resources</code> Folder</a></li>
  </ul>

  <h4 id="installing"><a href="#installing">Installing Custom Shaders</a></h4>

  <p>
    In order to install any custom shaders, you'll need to locate the <code>shaders</code> folder for your OpenMW installation.
  </p>

  <p>
    On Windows, this would be inside the <code>resources</code> folder next to the <code>openmw.exe</code> executable. On Linux, it might be next to the executable, or it might be at <code>/usr/share/openmw/resources/shaders</code> if you installed OpenMW via your system package manager. If you built OpenMW from source, it'd be at <code>$PREFIX/share/games/openmw/resources/shaders</code>. On macOS, find those at <code>/Applications/OpenMW.app/Contents/Resources/resources/shaders</code>.
  </p>

  {# <p>On macOS, find those at <code>/Applications/OpenMW.app/Contents/Resources/resources/shaders</code>.</p>  #}

  <p>
    Once you've located this folder, it's a good idea to make a backup of it in case you want to revert to the default shaders more easily for any reason.
  </p>

  <p>
    Now, installation is simple. Simply place any shader files (<code>.glsl</code>, or <code>.png</code> for the water normal) into the <code>shaders</code> folder, overwriting the defaults, and enjoy! Note that some shaders have settings requirements, so be sure to consult any available READMEs for them.
  </p>

  <h4 id="vtasteks-shaders"><a href="#vtasteks-shaders">Vtastek's Shaders</a></h4>

  <p>
    <a href="/mods/vtasteks-shaders/">Vtastek's Shaders</a> introduce vast changes to the game's interior and exterior lighting and shadows. If you're someone who prefers more realistic interior lighting, this will surely satisfy you, and the exterior changes add new life to familiar regions.
  </p>

  <p>
    Setup requires several steps, assuming you've already downloaded and extracted the files required as described <a href="/mods/vtasteks-shaders/#usage">here</a>:
  </p>

  <ul>
    <li>First: give the included <code>readme.html</code> file a close read and apply all changes that it mentions. Note step 5 but skip it and do step 6.</li>
    <li>Step 5 from that readme involves generating a plugin file that tweaks lights in cells. The usage of <code>tes3cmd</code> assumes a vanilla Morrowind setup in order to function properly. Read below for further details.</li>
  </ul>

  <h4 id="lightfixes-setup"><a href="#lightfixes-setup">Generating <code>LightFixes.esp</code>: Setup</a></h4>

  <p>
    This process involves using the <code>tes3cmd</code> tool to generate an <code>.esp</code> file that patches lighting in your game.
  </p>

  <p>
    The plugin included with the shader pack only covers Morrowind cells, so if you're using any mods that add cells you will want to generate a custom plugin to ensure those are covered.
  </p>

  <!-- TODO: rehost this script maybe
  <p>
    I've created a small bash script (<span class="bold">NOTE: Unfortunately, this tool does not support Windows at this time</span>) that will scan a given <code>openmw.cfg</code> file and produce a temporary folder that can be used for a <code>tes3cmd</code> environment, please download that from <a href="https://git.modding-openmw.com/Modding-OpenMW.com/vtasteks-light-fixes.sh/raw/branch/master/vtasteks-light-fixes.sh">here</a>, usage instructions are found <a href="https://git.modding-openmw.com/Modding-OpenMW.com/vtasteks-light-fixes.sh/src/branch/master/README.md#vtasteks-light-fixes-sh">here</a>.
  </p>
  -->

  <h4 id="lightfixes-plugin"><a href="#lightfixes-plugin">Generating <code>LightFixes.esp</code>: Get The Plugin</a></h4>

  <p>
    The steps below assume you have either done the setup steps mention above, or you have access to a properly configured vanilla Morrowind setup (meaning all of your plugins are in the <code>Data Files</code> folder and the <code>Morrowind.ini</code> file has a complete load order).
  </p>

  <p>
    Additionally, you should have already gone through the <code>readme.html</code> file included with <a href="/mods/vtasteks-shaders/">Vtastek's Shaders</a> and made any settings changes that it requires.
  </p>

  <p>
    Please also ensure you've downloaded all the required files for <a href="/mods/vtasteks-shaders/">Vtastek's Shaders</a>.
  </p>

  <ol>
    <li>Ensure you aren't loading any conflicting mods like <a href="/mods/true-lights-and-darkness-necro-edit/">True Lights and Darkness - Necro Edit</a>.</li>
    <li>
      Extract the <code>LightFixes.pl</code> file from the <code>openmwlightingbeta04.7z</code> archive and put it into your vanilla Morrowind's <code>Data Files</code> folder.
      <ul>
        <li>If you're using Windows, also copy the <code>tes3cmd.exe</code> file into there as well.</li>
        <li>If you're using Linux or macOS, make sure you've got a usable installation of <code>tes3cmd</code> on your system, preferrably in your <code>$PATH</code>.</li>
      </ul>
    </li>
    <li>Open a command prompt or terminal, <code>cd</code> into the vanilla Morrowind Data Files folder</li>
    <li>Run: <code>tes3cmd modify -program LightFixes.pl > LightFixes.log</code></li>
    <li>There will now be a file called <code>LightFixes.esp</code> in the vanilla Morrowind Data Files folder. Copy this out, rename it if desired, and so on according to your own setup.</li>
    <li>There will also be a file called <code>LightFixes.log</code> which will contain a record of all cells that were modified. It's a good idea to take a look at this to verify there were no major problems.</li>
    <li>Add that plugin to your load order just before your OMWLLF plugin.</li>
    <li>
      Copy the contents of the <code>shaders</code> folder from the <code>openmwlightingbeta04.7z</code> archive into the <code>shaders</code> folder of your OpenMW installation - overwrite all files when prompted.
      <ul>
        <li>It's not a bad idea to make a copy of this folder before you overwrite anything. If you don't do that, you can always uninstall and reinstall OpenMW to get the fresh files.</li>
        <li>The location of this depends on your OS; Windows users will find it right next to <code>openmw.exe</code>, Linux and macOS users may need to look elsewhere, though (for example, on a Linux system you may find it at: <code>/usr/share/games/openmw/resources/shaders</code> and on macOS these files are inside the OpenMW application: right click the program and select "Show Package Contents").</li>
        <li>See below for another idea about how to manage this.</li>
      </ul>
    </li>
    <li>
      Copy the contents of the <code>shaders1211.7z</code> archive into the <code>shaders</code> folder of your OpenMW installation - overwrite all files when prompted.
    </li>
  </ol>

  <p>
    At this point, you should now be ready to play. Enjoy!
  </p>

  <h4 id="managing-resources"><a href="#managing-resources">Managing OpenMW's <code>resources</code> Folder</a></h4>

  <p>
    Another option for handling mods such as Vtastek's shaders that require installing files into OpenMW's <code>resources</code> folder:
  </p>

  <p>
    Rather than overwrite the folder OpenMW comes with, copy it into your mods folder (e.g. <code>C:\games\OpenMWMods\resources</code>), place any modded files into there, add this line to your <code>openmw.cfg</code>:
  </p>

  <pre><code>resources=C:\games\OpenMWMods\resources</code></pre>

  <p>
    Keep in mind that files in this folder may change across OpenMW updates, so it is a good idea to make sure you update your copy as well.
  </p>

  {% include "tips-nav.html" with cur_page="tips-custom-shaders" %}

{% endblock %}
