from utilz.git import GIT_TAG


class VersionHeaderMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response["MOMW-Version"] = GIT_TAG
        return response
