import os
import subprocess

from django.conf import settings


def _get_tag():
    old_cwd = os.getcwd()
    os.chdir(os.path.join(settings.BASE_DIR, ".."))
    try:
        t = subprocess.Popen(["git", "describe", "--tags"], stdout=subprocess.PIPE)
        t_out = t.communicate()[0]
        os.chdir(old_cwd)
        return t_out.decode().strip("\n")
    except FileNotFoundError:
        return "No Tag"


GIT_TAG = _get_tag()
