# MOMW Website Release Process

This document describes the release process for the website.

## Release Pattern

The [modding-openmw.com](https://modding-openmw.com/) website is deployed on a git tag push.

The [staging.modding-openmw.com](https://staging.modding-openmw.com/) website is deployed on a push to the `master` branch.

The [beta.modding-openmw.com](https://beta.modding-openmw.com/) website is deployed on a push to the `beta` branch.

## Versioning

### Patch Release

A patch release contains only corrections to existing content.

### Minor Release

A minor release may contain:

- Content additions or removals for mod lists
- New pages or workflows on the site

### Major Release

A major release contains large changes to mod lists or new mod lists.

## Release Checklist

- [ ] Ensure you've got the latest [MOMW Tools Pack](https://modding-openmw.gitlab.io/momw-tools-pack/)
- [ ] Do `umo cache sync` and `umo install` for each mod list
- [ ] Configurate each mod list (use these args: `--beta --dev --no-customizations --run-validator`)

Example script for downloading and extracting each mod list:

```bash
for mod_list in $(momw-configurator list --beta --simple); do
    umo cache sync --beta $mod_list
    umo install $mod_list
done
```

Example script for configurating each mod list:

```bash
for mod_list in $(momw-configurator list --beta --simple); do
    momw-configurator config "$mod_list" --beta --dev --no-customizations --run-validator
done
```
